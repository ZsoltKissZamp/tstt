const webpack = require("webpack");
const merge = require("webpack-merge");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const base = require("./webpack.base.config");

module.exports = env => {
  return merge(base(env), {
    entry: {
      app: ["webpack-hot-middleware/client"]
    },
    devtool: "eval-source-map",
    devServer: {
      contentBase: "./dist",
      hot: true
    },
    plugins: [
      new CleanWebpackPlugin(["dist"]),
      new HtmlWebpackPlugin({
        title: "Hot Module Replacement"
      }),
      new webpack.NamedModulesPlugin(),
      new webpack.HotModuleReplacementPlugin()
    ]
  });
};
