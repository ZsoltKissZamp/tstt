/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/app.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/webpack/buildin/harmony-module.js":
/*!*******************************************!*\
  !*** (webpack)/buildin/harmony-module.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = function (originalModule) {
	if (!originalModule.webpackPolyfill) {
		var module = Object.create(originalModule);
		// module.parent = undefined by default
		if (!module.children) module.children = [];
		Object.defineProperty(module, "loaded", {
			enumerable: true,
			get: function get() {
				return module.l;
			}
		});
		Object.defineProperty(module, "id", {
			enumerable: true,
			get: function get() {
				return module.i;
			}
		});
		Object.defineProperty(module, "exports", {
			enumerable: true
		});
		module.webpackPolyfill = 1;
	}
	return module;
};

/***/ }),

/***/ "./src/app.js":
/*!********************!*\
  !*** ./src/app.js ***!
  \********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var babel_polyfill__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! babel-polyfill */ "babel-polyfill");
/* harmony import */ var babel_polyfill__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(babel_polyfill__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-dom */ "react-dom");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _app_App_jsx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/App.jsx */ "./src/app/App.jsx");







Promise.resolve(/*! require.ensure */).then((function () {
  react_dom__WEBPACK_IMPORTED_MODULE_2___default.a.render(react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_app_App_jsx__WEBPACK_IMPORTED_MODULE_3__["default"], null), document.getElementById("app"));
}).bind(null, __webpack_require__)).catch(__webpack_require__.oe);

/***/ }),

/***/ "./src/app/App.jsx":
/*!*************************!*\
  !*** ./src/app/App.jsx ***!
  \*************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var ramda__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ramda */ "ramda");
/* harmony import */ var ramda__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(ramda__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./model */ "./src/app/model/index.js");
/* harmony import */ var _initialState__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./initialState */ "./src/app/initialState.js");
/* harmony import */ var _view__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./view */ "./src/app/view/index.js");
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }








var makeId = function makeId(pack, randomLength) {
  var id = Math.floor(Math.random() * randomLength + 1);
  if (pack.includes(id)) {
    id = makeId(pack, randomLength);
  }
  return id;
};
var engin = function engin(len) {
  var n = len < 10 ? len === 7 ? 2 : 1 : len === 10 ? 2 : 3;
  var k = 0;
  if (len / 4 < 3) {
    if (len / 4 < 2) {
      if (len === 6) {
        k = 1;
      }
      if (len === 7) {
        k = 3;
      }
      if (len === 8) {
        k = 4;
      }
    } else if (len === 11) {
      k = 4;
    } else {
      k = 5;
    }
  } else k = 6;
  return ramda__WEBPACK_IMPORTED_MODULE_1__["map"](function (v) {
    return [v, v + n > len ? v + n - len : v + n, len === 6 ? v - 3 < 1 ? v - 3 + len : v - 3 : v - 2 < 1 ? v - 2 + len : v - 2, v - k < 1 ? v - k + len : v - k];
  }, ramda__WEBPACK_IMPORTED_MODULE_1__["range"](1, len + 1));
};

var App = function (_React$Component) {
  _inherits(App, _React$Component);

  function App(props) {
    _classCallCheck(this, App);

    var _this = _possibleConstructorReturn(this, (App.__proto__ || Object.getPrototypeOf(App)).call(this, props));

    _this.toggleTeamList = function (teamList) {
      return _this.setState({ teamList: teamList });
    };

    _this.togglePlayers = function (players) {
      return _this.setState({ players: players });
    };

    _this.toggleGames = function (games) {
      return _this.setState({ games: games });
    };

    _this.toggleTab = function (tab) {
      return _this.setState({ tab: tab });
    };

    _this.toggleStage = function (stage) {
      return _this.setState({ stage: stage });
    };

    _this.toggleModal = function (modal) {
      return _this.setState({ modal: modal });
    };

    _this.toggleUpdate = function (update) {
      return _this.setState({ update: update });
    };

    _this.toggleProgress = function (game) {
      return _this.setState({ game: game });
    };

    _this.toggleCounter = function (counter) {
      return _this.setState({ counter: counter });
    };

    _this.toggleChoice = function (choice) {
      return _this.setState({ choice: choice });
    };

    _this.toggleSortedPlayerList = function (sortedPlayerList) {
      return _this.setState({ sortedPlayerList: sortedPlayerList });
    };

    _this.checked = function (cellInfo) {
      var _this$state = _this.state,
          modal = _this$state.modal,
          game = _this$state.game;

      return modal && cellInfo.column.id === game.column.id && cellInfo.viewIndex === game.viewIndex ? "checked" : "";
    };

    _this.updatePlayer = function (player) {
      return _this.setState(function (state) {
        return _extends({}, state, {
          players: _extends({}, state.players, _defineProperty({}, player.id, player))
        });
      });
    };

    _this.updateGame = function (game) {
      return _this.setState(function (state) {
        return _extends({}, state, {
          games: _extends({}, state.games, _defineProperty({}, game.id, game))
        });
      });
    };

    _this.addPlayer = function (name) {
      var player = _model__WEBPACK_IMPORTED_MODULE_2__["Player"].new(name);
      player.gameId = _this.len() + 1;
      _this.updatePlayer(player);
    };

    _this.onRemovePlayer = function (gameId) {
      var player = _this.getPlayer(gameId);
      var gamers = ramda__WEBPACK_IMPORTED_MODULE_1__["filter"](function (gamer) {
        return gamer !== player;
      }, _this.state.players);
      _this.togglePlayers(gamers);
    };

    _this.addGame = function (game) {
      var play = _model__WEBPACK_IMPORTED_MODULE_2__["Game"].new(game);
      _this.updateGame(play);
    };

    _this.getPlayer = function (id) {
      var players = _this.state.players;

      var player = {};
      Object.values(players).map(function (gamer) {
        if (gamer.gameId === id) {
          player = gamer;
        }
      });
      return player;
    };

    _this.getPlayersList = function () {
      return Object.values(_this.state.players).map(function (p) {
        return p;
      });
    };

    _this.len = function () {
      return _this.getPlayersList().length;
    };

    _this.getGamesList = function () {
      return Object.values(_this.state.games).map(function (g) {
        return g;
      });
    };

    _this.changeStage = function (stage) {
      _this.toggleGames({});
      if (!(stage % 2)) {
        if (stage === 4) {
          _this.makeSortedList(false);
        } else {
          _this.makeGame();
        }
        _this.toggleTab(2);
      }
      _this.toggleStage(stage);
    };

    _this.makeUpdate = function (cellInfo) {
      var _cellInfo$original = cellInfo.original,
          black = _cellInfo$original.black,
          red = _cellInfo$original.red;
      var stage = _this.state.stage;


      if (stage === 4 && black !== red) {
        return {};
      } else {
        _this.toggleUpdate(black === red ? false : { black: black, red: red });
        _this.toggleProgress(cellInfo);
        _this.toggleModal(true);
      }
    };

    _this.makeGame = function () {
      var _this$state2 = _this.state,
          players = _this$state2.players,
          len = _this$state2.len;

      var pack = [];
      engin(len()).forEach(function (element, ind) {
        _this.addGame({
          id: ind + 1,
          club: element[0],
          spade: element[1],
          heart: element[2],
          diamond: element[3]
        });
      });
      _this.togglePlayers(ramda__WEBPACK_IMPORTED_MODULE_1__["mapObjIndexed"](function (player) {
        var id = makeId(pack, len());
        pack = [].concat(_toConsumableArray(pack), [id]);
        player.gameId = id;
        return player;
      }, players));
      _this.toggleCounter(len());
    };

    _this.makePoints = function (selectedOption) {
      var _this$state3 = _this.state,
          counter = _this$state3.counter,
          game = _this$state3.game,
          update = _this$state3.update,
          players = _this$state3.players,
          games = _this$state3.games;

      var play = game.original;
      var columName = game.column.id;
      [play.club, play.spade, play.heart, play.diamond].forEach(function (id, index) {
        _this.updatePlayer(_this.makePlayersPoints({ id: id, index: index, selectedOption: selectedOption }));
      });
      columName === "red" ? play.black = 6 : play.red = 6;
      play[columName] = selectedOption;
      !update && _this.toggleCounter(counter + 1);
      _this.toggleProgress(false);
      _this.updateGame(play);
      if (counter === _this.getGamesList().length + 1) {
        var data = {
          players: players,
          games: games
        };
        localStorage.setItem("data", JSON.stringify(data));
        _this.toggleUpdate(false);
        _this.changeStage(3);
        _this.toggleTab(1);
        _this.toggleCounter(1);
        _this.toggleModal(true);
      }
    };

    _this.makePlayersPoints = function (_ref) {
      var id = _ref.id,
          index = _ref.index,
          selectedOption = _ref.selectedOption;
      var _this$state4 = _this.state,
          update = _this$state4.update,
          game = _this$state4.game;

      var columName = game.column.id;
      var v = 6 - Number(selectedOption);
      var gamer = _this.getPlayer(id);
      if (update) {
        var black = update.black,
            red = update.red;

        var diff = black - red;
        if (columName === "red") {
          return _model__WEBPACK_IMPORTED_MODULE_2__["Player"].updateRed(index, gamer, diff, v);
        } else {
          return _model__WEBPACK_IMPORTED_MODULE_2__["Player"].updateBlack(index, gamer, diff, v);
        }
      } else {
        if (columName === "red") {
          return _model__WEBPACK_IMPORTED_MODULE_2__["Player"].red(index, gamer, v);
        } else {
          return _model__WEBPACK_IMPORTED_MODULE_2__["Player"].black(index, gamer, v);
        }
      }
    };

    _this.makeKoPreGame = function (selectedOption) {
      var game = _this.state.game;

      var columName = game.column.id;
      var play = game.original;
      var list = {
        winners: [],
        loosers: []
      };
      var club = _this.getPlayer(play.club);
      var spade = _this.getPlayer(play.spade);
      var heart = _this.getPlayer(play.heart);
      var diamond = _this.getPlayer(play.diamond);

      if (columName === "red") {
        list.winners = [club, spade];
        list.loosers = [heart, diamond];
        play.black = 6;
      } else {
        list.winners = [heart, diamond];
        list.lossers = [club, spade];
        play.red = 6;
      }
      for (var i = 0; i < 2; i++) {
        var p = list.loosers[i];
        p.out = true;
        _this.updatePlayer(p);
      }
      play[columName] = selectedOption;
      _this.toggleProgress(false);
      _this.toggleChoice(true);
      _this.makeSortedList(list.winners);
      _this.updateGame(play);
    };

    _this.makeSortedList = function (list) {
      var sortedPlayerList = _this.state.sortedPlayerList;

      var _sortedPlayerList = !list ? ramda__WEBPACK_IMPORTED_MODULE_1__["sortWith"]([ramda__WEBPACK_IMPORTED_MODULE_1__["descend"](ramda__WEBPACK_IMPORTED_MODULE_1__["prop"]("wins")), ramda__WEBPACK_IMPORTED_MODULE_1__["descend"](ramda__WEBPACK_IMPORTED_MODULE_1__["prop"]("points"))])(_this.getPlayersList()) : [].concat(_toConsumableArray(list), _toConsumableArray(sortedPlayerList));
      if (sortedPlayerList.length < 2 && list) {
        _this.makeWin();
      } else {
        _this.makeTeam(_sortedPlayerList);
        _this.toggleChoice(true);
      }
      _this.toggleSortedPlayerList(_sortedPlayerList);
    };

    _this.makeTeam = function (sortedPlayerList) {
      var counter = _this.state.counter;

      var len = sortedPlayerList.length;
      var l = sortedPlayerList;
      var gamers = [];
      for (var i = 1; i < 3; i++) {
        var _sortedPlayerList$pop = sortedPlayerList.pop(),
            gameId = _sortedPlayerList$pop.gameId,
            name = _sortedPlayerList$pop.name;

        gamers.push({
          value: {
            id: gameId,
            ind: gamers.length,
            i: counter
          },
          label: name
        });
      }
      if (len % 4 === 0 && len / 4 === 0) {
        for (var _i = 1; _i < 3; _i++) {
          var _sortedPlayerList$shi = sortedPlayerList.shift(),
              _gameId = _sortedPlayerList$shi.gameId,
              _name = _sortedPlayerList$shi.name;

          gamers.push({
            value: {
              id: _gameId,
              ind: gamers.length,
              i: counter
            },
            label: _name
          });
        }
      } else {
        for (var _i2 = 1; _i2 < 3; _i2++) {
          var _sortedPlayerList$pop2 = sortedPlayerList.pop(),
              _gameId2 = _sortedPlayerList$pop2.gameId,
              _name2 = _sortedPlayerList$pop2.name;

          gamers.push({
            value: {
              id: _gameId2,
              ind: gamers.length,
              i: counter
            },
            label: _name2
          });
        }
        console.log(len % 4 === 0 || len / 4 === 0);
      }
      _this.toggleCounter(counter + 1);
      _this.toggleTeamList(gamers);
      _this.toggleSortedPlayerList(l);
    };

    _this.makeWin = function () {
      var _this$state5 = _this.state,
          players = _this$state5.players,
          games = _this$state5.games;

      var data = {
        players: players,
        games: games
      };
      localStorage.setItem("koPlay", JSON.stringify(data));
      _this.toggleStage(5);
    };

    _this.onChangeSelect = function (selectedOption) {
      _this.state.stage < 4 ? _this.makePoints(selectedOption) : _this.makeKoPreGame(selectedOption);
      _this.toggleModal(false);
    };

    _this.onChangeSelected = function (selectedOption) {
      var _this$state6 = _this.state,
          teamList = _this$state6.teamList,
          game = _this$state6.game;
      var id = selectedOption.id,
          ind = selectedOption.ind,
          i = selectedOption.i;

      var nextId = ind < 2 ? Math.abs(ind - 1) : game ? 3 - ind : 5 - ind;
      if (game) {
        var club = game.club,
            spade = game.spade;

        var koGame = {
          id: i,
          heart: id,
          diamond: teamList[nextId].value.id,
          club: club,
          spade: spade
        };
        _this.addGame(koGame);
        _this.toggleChoice(false);
        _this.toggleProgress(false);
        _this.toggleTeamList([]);
      } else {
        var black = {
          club: id,
          spade: teamList[nextId].value.id
        };
        var forDeletion = [selectedOption.ind, nextId];
        var teams = teamList.filter(function (item) {
          return !forDeletion.includes(item.value.ind);
        });
        _this.toggleProgress(black);
        _this.toggleTeamList(teams);
      }
    };

    _this.state = _extends({}, _initialState__WEBPACK_IMPORTED_MODULE_3__["initialState"], {
      addPlayer: _this.addPlayer,
      updatePlayer: _this.updatePlayer,
      getPlayer: _this.getPlayer,
      getGamesList: _this.getGamesList,
      getPlayersList: _this.getPlayersList,
      makeUpdate: _this.makeUpdate,
      onChangeSelect: _this.onChangeSelect,
      onChangeSelected: _this.onChangeSelected,
      onRemovePlayer: _this.onRemovePlayer,
      toggleTab: _this.toggleTab,
      changeStage: _this.changeStage,
      len: _this.len,
      checked: _this.checked
    });
    return _this;
  }

  _createClass(App, [{
    key: "render",
    value: function render() {
      var _state = this.state,
          tab = _state.tab,
          stage = _state.stage,
          choice = _state.choice,
          teamList = _state.teamList,
          sortedPlayerList = _state.sortedPlayerList;

      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(
        _view__WEBPACK_IMPORTED_MODULE_4__["Context"].Provider,
        { value: this.state },
        react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_view__WEBPACK_IMPORTED_MODULE_4__["Header"], null),
        !(stage % 2) ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_view__WEBPACK_IMPORTED_MODULE_4__["Tournament"], { tab: tab, choice: choice, teamList: teamList }) : stage === 5 ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_view__WEBPACK_IMPORTED_MODULE_4__["WinnerForm"], { sortedPlayerList: sortedPlayerList }) : react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_view__WEBPACK_IMPORTED_MODULE_4__["Register"], null)
      );
    }
  }, {
    key: "__reactstandin__regenerateByEval",
    // @ts-ignore
    value: function __reactstandin__regenerateByEval(key, code) {
      // @ts-ignore
      this[key] = eval(code);
    }
  }]);

  return App;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.Component);

var _default = App;


/* harmony default export */ __webpack_exports__["default"] = (_default);
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(makeId, "makeId", "/home/spinoble/Work/tstt/src/app/App.jsx");
  reactHotLoader.register(engin, "engin", "/home/spinoble/Work/tstt/src/app/App.jsx");
  reactHotLoader.register(App, "App", "/home/spinoble/Work/tstt/src/app/App.jsx");
  reactHotLoader.register(_default, "default", "/home/spinoble/Work/tstt/src/app/App.jsx");
  leaveModule(module);
})();

;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./src/app/initialState.js":
/*!*********************************!*\
  !*** ./src/app/initialState.js ***!
  \*********************************/
/*! exports provided: initialState */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "initialState", function() { return initialState; });
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./util */ "./src/app/util.js");
(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();

(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();



var dev = true;

var initialState = {
  players: dev ? _util__WEBPACK_IMPORTED_MODULE_0__["default"] : {},
  games: {},
  player: {},
  game: {},
  sortedPlayerList: [],
  teamList: [],
  stage: 1,
  tab: 1,
  counter: 1,
  modal: false,
  update: false,
  choice: false
};
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(dev, "dev", "/home/spinoble/Work/tstt/src/app/initialState.js");
  reactHotLoader.register(initialState, "initialState", "/home/spinoble/Work/tstt/src/app/initialState.js");
  leaveModule(module);
})();

;
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(dev, 'dev', '/home/spinoble/Work/tstt/src/app/initialState.js');
  reactHotLoader.register(initialState, 'initialState', '/home/spinoble/Work/tstt/src/app/initialState.js');
  leaveModule(module);
})();

;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./src/app/model/Game.js":
/*!*******************************!*\
  !*** ./src/app/model/Game.js ***!
  \*******************************/
/*! exports provided: Game */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Game", function() { return Game; });
/* harmony import */ var uuid__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! uuid */ "uuid");
/* harmony import */ var uuid__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(uuid__WEBPACK_IMPORTED_MODULE_0__);
(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();

(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();



var Game = {
  new: function _new() {
    var game = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    return {
      id: Object(uuid__WEBPACK_IMPORTED_MODULE_0__["v4"])(),
      club: game.club,
      spade: game.spade,
      heart: game.heart,
      diamond: game.diamond,
      black: 0,
      red: 0,
      gameId: game.id
    };
  }
};
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(Game, "Game", "/home/spinoble/Work/tstt/src/app/model/Game.js");
  leaveModule(module);
})();

;
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(Game, 'Game', '/home/spinoble/Work/tstt/src/app/model/Game.js');
  leaveModule(module);
})();

;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./src/app/model/Player.js":
/*!*********************************!*\
  !*** ./src/app/model/Player.js ***!
  \*********************************/
/*! exports provided: Player */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Player", function() { return Player; });
/* harmony import */ var uuid__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! uuid */ "uuid");
/* harmony import */ var uuid__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(uuid__WEBPACK_IMPORTED_MODULE_0__);
(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();



var Player = {
  new: function _new(name) {
    return {
      id: Object(uuid__WEBPACK_IMPORTED_MODULE_0__["v4"])(),
      name: name,
      wins: 0,
      diffs: 0,
      points: 0,
      gameId: 0,
      out: false
    };
  },
  black: function black(index, gamer, v) {
    var player = index >= 2 ? _extends({}, gamer, {
      diffs: gamer.diffs + v,
      wins: gamer.wins + 1
    }) : _extends({}, gamer, {
      diffs: gamer.diffs - v
    });
    player.points = player.diffs + player.wins;
    return player;
  },
  red: function red(index, gamer, v) {
    var player = index < 2 ? _extends({}, gamer, {
      diffs: gamer.diffs + v,
      wins: gamer.wins + 1
    }) : _extends({}, gamer, {
      diffs: gamer.diffs - v
    });
    player.points = player.diffs + player.wins;
    return player;
  },
  updateRed: function updateRed(index, gamer, diff, v) {
    var player = index < 2 ? _extends({}, gamer, {
      diffs: gamer.diffs - diff + v,
      wins: diff < 0 ? gamer.wins + 1 : gamer.wins
    }) : _extends({}, gamer, {
      diffs: gamer.diffs + diff - v,
      wins: diff < 0 ? gamer.wins - 1 : gamer.wins
    });
    player.points = player.diffs + player.wins;
    return player;
  },
  updateBlack: function updateBlack(index, gamer, diff, v) {
    var player = index >= 2 ? _extends({}, gamer, {
      diffs: gamer.diffs + diff + v,
      wins: diff > 0 ? gamer.wins + 1 : gamer.wins
    }) : _extends({}, gamer, {
      diffs: gamer.diffs - diff - v,
      wins: diff > 0 ? gamer.wins - 1 : gamer.wins
    });
    player.points = player.diffs + player.wins;
    return player;
  }
};
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(Player, "Player", "/home/spinoble/Work/tstt/src/app/model/Player.js");
  leaveModule(module);
})();

;
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(_extends, '_extends', '/home/spinoble/Work/tstt/src/app/model/Player.js');
  reactHotLoader.register(Player, 'Player', '/home/spinoble/Work/tstt/src/app/model/Player.js');
  leaveModule(module);
})();

;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./src/app/model/index.js":
/*!********************************!*\
  !*** ./src/app/model/index.js ***!
  \********************************/
/*! exports provided: Player, Game */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Player__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Player */ "./src/app/model/Player.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Player", function() { return _Player__WEBPACK_IMPORTED_MODULE_0__["Player"]; });

/* harmony import */ var _Game__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Game */ "./src/app/model/Game.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Game", function() { return _Game__WEBPACK_IMPORTED_MODULE_1__["Game"]; });




/***/ }),

/***/ "./src/app/util.js":
/*!*************************!*\
  !*** ./src/app/util.js ***!
  \*************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var _model__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./model */ "./src/app/model/index.js");
(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true });
  } else {
    obj[key] = value;
  }return obj;
}



var namen = ["aa", "bb", "cc", "dd", "ee", "ff", "gg", "hh", "ii", "jj", "kk"];
var demoPlayers = {};

var _iteratorNormalCompletion = true;
var _didIteratorError = false;
var _iteratorError = undefined;

try {
  for (var _iterator = namen[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
    var name = _step.value;

    var player = _model__WEBPACK_IMPORTED_MODULE_0__["Player"].new(name);
    demoPlayers = _extends({}, demoPlayers, _defineProperty({}, player.id, player));
  }
} catch (err) {
  _didIteratorError = true;
  _iteratorError = err;
} finally {
  try {
    if (!_iteratorNormalCompletion && _iterator.return) {
      _iterator.return();
    }
  } finally {
    if (_didIteratorError) {
      throw _iteratorError;
    }
  }
}

var _default = demoPlayers;
var _default2 = _default;
/* harmony default export */ __webpack_exports__["default"] = (_default2);
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(namen, "namen", "/home/spinoble/Work/tstt/src/app/util.js");
  reactHotLoader.register(demoPlayers, "demoPlayers", "/home/spinoble/Work/tstt/src/app/util.js");
  reactHotLoader.register(_default, "default", "/home/spinoble/Work/tstt/src/app/util.js");
  leaveModule(module);
})();

;
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(_extends, "_extends", "/home/spinoble/Work/tstt/src/app/util.js");
  reactHotLoader.register(_defineProperty, "_defineProperty", "/home/spinoble/Work/tstt/src/app/util.js");
  reactHotLoader.register(namen, "namen", "/home/spinoble/Work/tstt/src/app/util.js");
  reactHotLoader.register(demoPlayers, "demoPlayers", "/home/spinoble/Work/tstt/src/app/util.js");
  reactHotLoader.register(_iteratorNormalCompletion, "_iteratorNormalCompletion", "/home/spinoble/Work/tstt/src/app/util.js");
  reactHotLoader.register(_didIteratorError, "_didIteratorError", "/home/spinoble/Work/tstt/src/app/util.js");
  reactHotLoader.register(_iteratorError, "_iteratorError", "/home/spinoble/Work/tstt/src/app/util.js");
  reactHotLoader.register(_iterator, "_iterator", "/home/spinoble/Work/tstt/src/app/util.js");
  reactHotLoader.register(_step, "_step", "/home/spinoble/Work/tstt/src/app/util.js");
  reactHotLoader.register(name, "name", "/home/spinoble/Work/tstt/src/app/util.js");
  reactHotLoader.register(player, "player", "/home/spinoble/Work/tstt/src/app/util.js");
  reactHotLoader.register(_default, "_default", "/home/spinoble/Work/tstt/src/app/util.js");
  reactHotLoader.register(_default2, "default", "/home/spinoble/Work/tstt/src/app/util.js");
  leaveModule(module);
})();

;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./src/app/view/Header.js":
/*!********************************!*\
  !*** ./src/app/view/Header.js ***!
  \********************************/
/*! exports provided: Header */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Header", function() { return Header; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components */ "./src/app/view/components/index.js");
(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();

(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();




var Header = function Header() {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", { className: "header" }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", null, "Table Soccer Tournament Tool"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", { className: "navi" }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_1__["GamePanel"], null)));
};
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(Header, "Header", "/home/spinoble/Work/tstt/src/app/view/Header.js");
  leaveModule(module);
})();

;
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(Header, "Header", "/home/spinoble/Work/tstt/src/app/view/Header.js");
  leaveModule(module);
})();

;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./src/app/view/Register.js":
/*!**********************************!*\
  !*** ./src/app/view/Register.js ***!
  \**********************************/
/*! exports provided: Register */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Register", function() { return Register; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components */ "./src/app/view/components/index.js");
(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();

(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();




var Register = function Register() {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", { className: "RegisterView" }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_1__["AddPlayerForm"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_1__["PlayerTable"], null));
};
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(Register, "Register", "/home/spinoble/Work/tstt/src/app/view/Register.js");
  leaveModule(module);
})();

;
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(Register, "Register", "/home/spinoble/Work/tstt/src/app/view/Register.js");
  leaveModule(module);
})();

;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./src/app/view/Tournament.js":
/*!************************************!*\
  !*** ./src/app/view/Tournament.js ***!
  \************************************/
/*! exports provided: Tournament */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tournament", function() { return Tournament; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components */ "./src/app/view/components/index.js");
(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();

(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();






var Tournament = function Tournament(_ref) {
  var tab = _ref.tab,
      teamList = _ref.teamList,
      choice = _ref.choice;

  return choice ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_2__["ChoiceForm"], { state: teamList.length === 2 ? false : true }) : tab === 1 ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_2__["PlayerTable"], null) : react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_2__["GameTable"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_2__["UpdateGameModal"], null));
};

Tournament.propTypes = {
  tab: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.number.isRequired,
  choice: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool.isRequired,
  teamList: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.array.isRequired
};
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(Tournament, "Tournament", "/home/spinoble/Work/tstt/src/app/view/Tournament.js");
  leaveModule(module);
})();

;
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(Tournament, "Tournament", "/home/spinoble/Work/tstt/src/app/view/Tournament.js");
  leaveModule(module);
})();

;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./src/app/view/WinnerForm.js":
/*!************************************!*\
  !*** ./src/app/view/WinnerForm.js ***!
  \************************************/
/*! exports provided: WinnerForm */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WinnerForm", function() { return WinnerForm; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components */ "./src/app/view/components/index.js");
(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();

(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();




var WinnerForm = function WinnerForm(_ref) {
  var sortedPlayerList = _ref.sortedPlayerList;

  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", { className: "carry" }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, "Congratulation Today the winner are: "), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", { className: "holder" }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_2__["NameLabel"], { name: sortedPlayerList[0].name }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components__WEBPACK_IMPORTED_MODULE_2__["NameLabel"], { name: sortedPlayerList[1].name })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", null, "Get Pdf"));
};
WinnerForm.propTypes = {
  sortedPlayerList: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.array
};
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(WinnerForm, "WinnerForm", "/home/spinoble/Work/tstt/src/app/view/WinnerForm.js");
  leaveModule(module);
})();

;
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(WinnerForm, "WinnerForm", "/home/spinoble/Work/tstt/src/app/view/WinnerForm.js");
  leaveModule(module);
})();

;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./src/app/view/components/AddPlayerForm.js":
/*!**************************************************!*\
  !*** ./src/app/view/components/AddPlayerForm.js ***!
  \**************************************************/
/*! exports provided: AddPlayerForm */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddPlayerForm", function() { return AddPlayerForm; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var ramda_adjunct__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ramda-adjunct */ "ramda-adjunct");
/* harmony import */ var ramda_adjunct__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(ramda_adjunct__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Context__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Context */ "./src/app/view/components/Context.js");
/* harmony import */ var _NameInput__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./NameInput */ "./src/app/view/components/NameInput.js");
var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();

var _createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
    }
  }return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
  };
}();

(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }return call && ((typeof call === "undefined" ? "undefined" : _typeof(call)) === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + (typeof superClass === "undefined" ? "undefined" : _typeof(superClass)));
  }subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}






var AddPlayerForm = function (_React$Component) {
  _inherits(AddPlayerForm, _React$Component);

  function AddPlayerForm(props) {
    _classCallCheck(this, AddPlayerForm);

    var _this = _possibleConstructorReturn(this, (AddPlayerForm.__proto__ || Object.getPrototypeOf(AddPlayerForm)).call(this, props));

    _this.handleChange = function (event) {
      return _this.setState({ name: event.target.value });
    };

    _this.resetState = function () {
      return _this.setState({ name: "" });
    };

    _this.state = { name: "" };
    _this.handleChange = _this.handleChange.bind(_this);
    _this.resetState = _this.resetState.bind(_this);
    return _this;
  }

  _createClass(AddPlayerForm, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      var name = this.state.name;

      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Context__WEBPACK_IMPORTED_MODULE_2__["Context"].Consumer, null, function (_ref) {
        var addPlayer = _ref.addPlayer,
            stage = _ref.stage,
            len = _ref.len;

        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("form", {
          onSubmit: function onSubmit(event) {
            event.preventDefault();
            if (!Object(ramda_adjunct__WEBPACK_IMPORTED_MODULE_1__["isNilOrEmpty"])(name)) {
              addPlayer(name);
            }
            _this2.resetState();
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_NameInput__WEBPACK_IMPORTED_MODULE_3__["NameInput"], {
          aF: true,
          type: "text",
          v: name,
          oC: _this2.handleChange,
          d: stage === 3 && len() > 5 && !(len() % 2) ? true : false
        }));
      });
    }
  }, {
    key: "__reactstandin__regenerateByEval",
    // @ts-ignore
    value: function __reactstandin__regenerateByEval(key, code) {
      // @ts-ignore
      this[key] = eval(code);
    }
  }]);

  return AddPlayerForm;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.Component);
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(AddPlayerForm, "AddPlayerForm", "/home/spinoble/Work/tstt/src/app/view/components/AddPlayerForm.js");
  leaveModule(module);
})();

;
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(_createClass, "_createClass", "/home/spinoble/Work/tstt/src/app/view/components/AddPlayerForm.js");
  reactHotLoader.register(_classCallCheck, "_classCallCheck", "/home/spinoble/Work/tstt/src/app/view/components/AddPlayerForm.js");
  reactHotLoader.register(_possibleConstructorReturn, "_possibleConstructorReturn", "/home/spinoble/Work/tstt/src/app/view/components/AddPlayerForm.js");
  reactHotLoader.register(_inherits, "_inherits", "/home/spinoble/Work/tstt/src/app/view/components/AddPlayerForm.js");
  reactHotLoader.register(AddPlayerForm, "AddPlayerForm", "/home/spinoble/Work/tstt/src/app/view/components/AddPlayerForm.js");
  leaveModule(module);
})();

;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./src/app/view/components/ChoiceForm.js":
/*!***********************************************!*\
  !*** ./src/app/view/components/ChoiceForm.js ***!
  \***********************************************/
/*! exports provided: ChoiceForm */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChoiceForm", function() { return ChoiceForm; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _icons__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./icons */ "./src/app/view/components/icons/index.js");
/* harmony import */ var _TeamSelect__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./TeamSelect */ "./src/app/view/components/TeamSelect.js");
(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();

(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();







var ChoiceForm = function ChoiceForm(_ref) {
  var state = _ref.state;

  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", { className: "choice" }, state ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_icons__WEBPACK_IMPORTED_MODULE_2__["Club"], null) : react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_icons__WEBPACK_IMPORTED_MODULE_2__["Heart"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_TeamSelect__WEBPACK_IMPORTED_MODULE_3__["TeamSelect"], null));
};

ChoiceForm.propTypes = {
  state: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool
};
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(ChoiceForm, "ChoiceForm", "/home/spinoble/Work/tstt/src/app/view/components/ChoiceForm.js");
  leaveModule(module);
})();

;
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(ChoiceForm, "ChoiceForm", "/home/spinoble/Work/tstt/src/app/view/components/ChoiceForm.js");
  leaveModule(module);
})();

;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./src/app/view/components/Context.js":
/*!********************************************!*\
  !*** ./src/app/view/components/Context.js ***!
  \********************************************/
/*! exports provided: Context */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Context", function() { return Context; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _initialState__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../initialState */ "./src/app/initialState.js");
(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();

(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();



var Context = react__WEBPACK_IMPORTED_MODULE_0___default.a.createContext(_initialState__WEBPACK_IMPORTED_MODULE_1__["initialState"]);
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(Context, "Context", "/home/spinoble/Work/tstt/src/app/view/components/Context.js");
  leaveModule(module);
})();

;
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(Context, "Context", "/home/spinoble/Work/tstt/src/app/view/components/Context.js");
  leaveModule(module);
})();

;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./src/app/view/components/GamePanel.js":
/*!**********************************************!*\
  !*** ./src/app/view/components/GamePanel.js ***!
  \**********************************************/
/*! exports provided: GamePanel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GamePanel", function() { return GamePanel; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Context__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Context */ "./src/app/view/components/Context.js");
/* harmony import */ var _TogglerButton__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./TogglerButton */ "./src/app/view/components/TogglerButton.js");
(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();

(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();




var text = {
  button: {
    underMinimum: "Add Player",
    start: "Start Game",
    startKO: "Start KO Game",
    default: "Add or remove Player"
  },
  tab: {
    player: "Players",
    game: "Games"
  }
};
var GamePanel = function GamePanel() {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Context__WEBPACK_IMPORTED_MODULE_1__["Context"].Consumer, null, function (_ref) {
    var stage = _ref.stage,
        changeStage = _ref.changeStage,
        tab = _ref.tab,
        toggleTab = _ref.toggleTab,
        len = _ref.len;

    return stage === 1 ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_TogglerButton__WEBPACK_IMPORTED_MODULE_2__["TogglerButton"], {
      disabled: len() < 6 ? true : false,
      onClick: function onClick() {
        return changeStage(2);
      }
    }, len() < 6 ? text.button.underMinimum : text.button.start) : stage === 3 && !(len() % 2) ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_TogglerButton__WEBPACK_IMPORTED_MODULE_2__["TogglerButton"], { disabled: false, onClick: function onClick() {
        return changeStage(4);
      } }, text.button.startKO) : !(stage % 2) && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_TogglerButton__WEBPACK_IMPORTED_MODULE_2__["TogglerButton"], {
      disabled: tab === 2 ? false : true,
      onClick: function onClick() {
        return toggleTab(1);
      }
    }, text.tab.player), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_TogglerButton__WEBPACK_IMPORTED_MODULE_2__["TogglerButton"], {
      disabled: tab === 1 ? false : true,
      onClick: function onClick() {
        return toggleTab(2);
      }
    }, text.tab.game));
  });
};
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(text, "text", "/home/spinoble/Work/tstt/src/app/view/components/GamePanel.js");
  reactHotLoader.register(GamePanel, "GamePanel", "/home/spinoble/Work/tstt/src/app/view/components/GamePanel.js");
  leaveModule(module);
})();

;
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(text, "text", "/home/spinoble/Work/tstt/src/app/view/components/GamePanel.js");
  reactHotLoader.register(GamePanel, "GamePanel", "/home/spinoble/Work/tstt/src/app/view/components/GamePanel.js");
  leaveModule(module);
})();

;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./src/app/view/components/GameTable.js":
/*!**********************************************!*\
  !*** ./src/app/view/components/GameTable.js ***!
  \**********************************************/
/*! exports provided: GameTable */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GameTable", function() { return GameTable; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_table__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-table */ "react-table");
/* harmony import */ var react_table__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_table__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Context__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Context */ "./src/app/view/components/Context.js");
/* harmony import */ var _table__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./table */ "./src/app/view/components/table/index.js");
(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();

(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();







var GameTable = function GameTable() {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Context__WEBPACK_IMPORTED_MODULE_2__["Context"].Consumer, null, function (_ref) {
    var getGamesList = _ref.getGamesList;

    var columns = [_table__WEBPACK_IMPORTED_MODULE_3__["IdHead"], Object(_table__WEBPACK_IMPORTED_MODULE_3__["IconedHead"])("club"), Object(_table__WEBPACK_IMPORTED_MODULE_3__["IconedHead"])("spade"), Object(_table__WEBPACK_IMPORTED_MODULE_3__["IconedHead"])("heart"), Object(_table__WEBPACK_IMPORTED_MODULE_3__["IconedHead"])("diamond"), Object(_table__WEBPACK_IMPORTED_MODULE_3__["ScoreHead"])("black"), Object(_table__WEBPACK_IMPORTED_MODULE_3__["ScoreHead"])("red")];

    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_table__WEBPACK_IMPORTED_MODULE_1___default.a, {
      className: "center -striped bordered",
      showPagination: false,
      minRows: 1,
      data: getGamesList(),
      resizable: false,
      defaultSorted: [{
        id: "gameId",
        asc: true
      }],
      sortable: false,
      columns: columns
    });
  });
};
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(GameTable, "GameTable", "/home/spinoble/Work/tstt/src/app/view/components/GameTable.js");
  leaveModule(module);
})();

;
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(GameTable, "GameTable", "/home/spinoble/Work/tstt/src/app/view/components/GameTable.js");
  leaveModule(module);
})();

;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./src/app/view/components/NameInput.js":
/*!**********************************************!*\
  !*** ./src/app/view/components/NameInput.js ***!
  \**********************************************/
/*! exports provided: NameInput */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NameInput", function() { return NameInput; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();

(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();




var NameInput = function NameInput(_ref) {
  var aF = _ref.aF,
      v = _ref.v,
      oC = _ref.oC,
      d = _ref.d;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", { autoFocus: aF, type: "text", value: v, onChange: oC, disabled: d });
};

NameInput.propTypes = {
  aF: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool,
  v: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  oC: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func,
  d: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool
};
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(NameInput, "NameInput", "/home/spinoble/Work/tstt/src/app/view/components/NameInput.js");
  leaveModule(module);
})();

;
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(NameInput, "NameInput", "/home/spinoble/Work/tstt/src/app/view/components/NameInput.js");
  leaveModule(module);
})();

;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./src/app/view/components/NameLabel.js":
/*!**********************************************!*\
  !*** ./src/app/view/components/NameLabel.js ***!
  \**********************************************/
/*! exports provided: NameLabel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NameLabel", function() { return NameLabel; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();

(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();




var NameLabel = function NameLabel(_ref) {
  var name = _ref.name;

  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", { className: "winner" }, name);
};
NameLabel.propTypes = {
  name: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string.isRequired
};
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(NameLabel, "NameLabel", "/home/spinoble/Work/tstt/src/app/view/components/NameLabel.js");
  leaveModule(module);
})();

;
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(NameLabel, "NameLabel", "/home/spinoble/Work/tstt/src/app/view/components/NameLabel.js");
  leaveModule(module);
})();

;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./src/app/view/components/PlayerTable.js":
/*!************************************************!*\
  !*** ./src/app/view/components/PlayerTable.js ***!
  \************************************************/
/*! exports provided: PlayerTable */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PlayerTable", function() { return PlayerTable; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_table__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-table */ "react-table");
/* harmony import */ var react_table__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_table__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var ramda__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ramda */ "ramda");
/* harmony import */ var ramda__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(ramda__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _Context__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Context */ "./src/app/view/components/Context.js");
/* harmony import */ var _table__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./table */ "./src/app/view/components/table/index.js");
(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();

(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();

function _toConsumableArray(arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) {
      arr2[i] = arr[i];
    }return arr2;
  } else {
    return Array.from(arr);
  }
}










var PlayerTable = function PlayerTable() {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Context__WEBPACK_IMPORTED_MODULE_3__["Context"].Consumer, null, function (_ref) {
    var getPlayersList = _ref.getPlayersList,
        stage = _ref.stage;

    var columns = [_table__WEBPACK_IMPORTED_MODULE_4__["IdHead"], _table__WEBPACK_IMPORTED_MODULE_4__["UpdateNameHead"]];
    columns = stage % 2 ? [].concat(_toConsumableArray(columns), [_table__WEBPACK_IMPORTED_MODULE_4__["RemovePlayerHead"]]) : ramda__WEBPACK_IMPORTED_MODULE_2__["concat"](columns, _table__WEBPACK_IMPORTED_MODULE_4__["StatsHead"]);
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_table__WEBPACK_IMPORTED_MODULE_1___default.a, {
      className: "center -striped bordered " + (stage !== 2 ? stage !== 4 ? "suplied" : "" : ""),
      showPagination: false,
      minRows: 6,
      data: getPlayersList(),
      defaultSorted: stage > 2 ? [{
        id: "wins",
        desc: true
      }, {
        id: "points",
        desc: true
      }] : [{
        id: "gameId",
        asc: true
      }],
      columns: columns
    });
  });
};
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(PlayerTable, "PlayerTable", "/home/spinoble/Work/tstt/src/app/view/components/PlayerTable.js");
  leaveModule(module);
})();

;
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(_toConsumableArray, "_toConsumableArray", "/home/spinoble/Work/tstt/src/app/view/components/PlayerTable.js");
  reactHotLoader.register(PlayerTable, "PlayerTable", "/home/spinoble/Work/tstt/src/app/view/components/PlayerTable.js");
  leaveModule(module);
})();

;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./src/app/view/components/ResultSelect.js":
/*!*************************************************!*\
  !*** ./src/app/view/components/ResultSelect.js ***!
  \*************************************************/
/*! exports provided: ResultSelect */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResultSelect", function() { return ResultSelect; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_select__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-select */ "react-select");
/* harmony import */ var react_select__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_select__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Context__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Context */ "./src/app/view/components/Context.js");
(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }return target;
};

(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();





var customStyles = {
  option: function option(base, state) {
    return _extends({}, base, {
      borderBottom: "1px dotted pink",
      color: state.isFullscreen ? "red" : "blue",
      padding: 5
    });
  },
  control: function control() {
    return {
      // none of react-selects styles are passed to <View />
      display: "none"
    };
  },
  singleValue: function singleValue(base, state) {
    var opacity = state.isDisabled ? 0.5 : 1;
    var transition = "opacity 300ms";

    return _extends({}, base, { opacity: opacity, transition: transition });
  }
};

var options = [{ value: 0, label: "0" }, { value: 1, label: "1" }, { value: 2, label: "2" }, { value: 3, label: "3" }, { value: 4, label: "4" }, { value: 5, label: "5" }];

var ResultSelect = function ResultSelect() {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Context__WEBPACK_IMPORTED_MODULE_2__["Context"].Consumer, null, function (_ref) {
    var onChangeSelect = _ref.onChangeSelect;

    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_select__WEBPACK_IMPORTED_MODULE_1___default.a, {
      styles: customStyles,
      menuIsOpen: true,
      onChange: function onChange(e) {
        return onChangeSelect(e.value);
      },
      options: options
    });
  });
};
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(customStyles, "customStyles", "/home/spinoble/Work/tstt/src/app/view/components/ResultSelect.js");
  reactHotLoader.register(options, "options", "/home/spinoble/Work/tstt/src/app/view/components/ResultSelect.js");
  reactHotLoader.register(ResultSelect, "ResultSelect", "/home/spinoble/Work/tstt/src/app/view/components/ResultSelect.js");
  leaveModule(module);
})();

;
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(_extends, "_extends", "/home/spinoble/Work/tstt/src/app/view/components/ResultSelect.js");
  reactHotLoader.register(customStyles, "customStyles", "/home/spinoble/Work/tstt/src/app/view/components/ResultSelect.js");
  reactHotLoader.register(options, "options", "/home/spinoble/Work/tstt/src/app/view/components/ResultSelect.js");
  reactHotLoader.register(ResultSelect, "ResultSelect", "/home/spinoble/Work/tstt/src/app/view/components/ResultSelect.js");
  leaveModule(module);
})();

;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./src/app/view/components/TeamSelect.js":
/*!***********************************************!*\
  !*** ./src/app/view/components/TeamSelect.js ***!
  \***********************************************/
/*! exports provided: TeamSelect */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TeamSelect", function() { return TeamSelect; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_select__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-select */ "react-select");
/* harmony import */ var react_select__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_select__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Context__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Context */ "./src/app/view/components/Context.js");
(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();

(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();





var customTeam = {
  control: function control() {
    return {
      display: "none"
    };
  }
};
var TeamSelect = function TeamSelect() {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Context__WEBPACK_IMPORTED_MODULE_2__["Context"].Consumer, null, function (_ref) {
    var teamList = _ref.teamList,
        onChangeSelected = _ref.onChangeSelected;

    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_select__WEBPACK_IMPORTED_MODULE_1___default.a, {
      styles: customTeam,
      menuIsOpen: true,
      onChange: function onChange(e) {
        return onChangeSelected(e.value);
      },
      options: teamList
    });
  });
};
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(customTeam, "customTeam", "/home/spinoble/Work/tstt/src/app/view/components/TeamSelect.js");
  reactHotLoader.register(TeamSelect, "TeamSelect", "/home/spinoble/Work/tstt/src/app/view/components/TeamSelect.js");
  leaveModule(module);
})();

;
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(customTeam, "customTeam", "/home/spinoble/Work/tstt/src/app/view/components/TeamSelect.js");
  reactHotLoader.register(TeamSelect, "TeamSelect", "/home/spinoble/Work/tstt/src/app/view/components/TeamSelect.js");
  leaveModule(module);
})();

;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./src/app/view/components/TogglerButton.js":
/*!**************************************************!*\
  !*** ./src/app/view/components/TogglerButton.js ***!
  \**************************************************/
/*! exports provided: TogglerButton */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TogglerButton", function() { return TogglerButton; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();

(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();




var TogglerButton = function TogglerButton(_ref) {
  var disabled = _ref.disabled,
      onClick = _ref.onClick,
      children = _ref.children;

  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    className: "inputname",
    type: "button",
    disabled: disabled,
    onClick: onClick
  }, children);
};
TogglerButton.propTypes = {
  disabled: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool.isRequired,
  onClick: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func.isRequired,
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string.isRequired
};
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(TogglerButton, "TogglerButton", "/home/spinoble/Work/tstt/src/app/view/components/TogglerButton.js");
  leaveModule(module);
})();

;
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(TogglerButton, "TogglerButton", "/home/spinoble/Work/tstt/src/app/view/components/TogglerButton.js");
  leaveModule(module);
})();

;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./src/app/view/components/UpdateGameModal.js":
/*!****************************************************!*\
  !*** ./src/app/view/components/UpdateGameModal.js ***!
  \****************************************************/
/*! exports provided: UpdateGameModal */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateGameModal", function() { return UpdateGameModal; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_modal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-modal */ "react-modal");
/* harmony import */ var react_modal__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_modal__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _ResultSelect__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ResultSelect */ "./src/app/view/components/ResultSelect.js");
/* harmony import */ var _Context__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Context */ "./src/app/view/components/Context.js");
(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();

(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();






var modalStyle = {
  overlay: {
    backgroundColor: "rgba(85, 139, 27, 0.55)"
  },
  content: {
    position: "fixed",
    top: "5rem",
    width: "10%",
    left: "none",
    right: 0,
    border: 0,
    pading: 0,
    background: ""
  }
};

react_modal__WEBPACK_IMPORTED_MODULE_1___default.a.setAppElement("#app");

var UpdateGameModal = function UpdateGameModal() {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Context__WEBPACK_IMPORTED_MODULE_3__["Context"].Consumer, null, function (_ref) {
    var modal = _ref.modal;

    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_modal__WEBPACK_IMPORTED_MODULE_1___default.a, {
      isOpen: modal,
      style: modalStyle,
      shouldCloseOnOverlayClick: false,
      ariaHideApp: true
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ResultSelect__WEBPACK_IMPORTED_MODULE_2__["ResultSelect"], null));
  });
};
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(modalStyle, "modalStyle", "/home/spinoble/Work/tstt/src/app/view/components/UpdateGameModal.js");
  reactHotLoader.register(UpdateGameModal, "UpdateGameModal", "/home/spinoble/Work/tstt/src/app/view/components/UpdateGameModal.js");
  leaveModule(module);
})();

;
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(modalStyle, "modalStyle", "/home/spinoble/Work/tstt/src/app/view/components/UpdateGameModal.js");
  reactHotLoader.register(UpdateGameModal, "UpdateGameModal", "/home/spinoble/Work/tstt/src/app/view/components/UpdateGameModal.js");
  leaveModule(module);
})();

;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./src/app/view/components/icons/Club.js":
/*!***********************************************!*\
  !*** ./src/app/view/components/icons/Club.js ***!
  \***********************************************/
/*! exports provided: Club */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Club", function() { return Club; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();

(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();



var Club = function Club() {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("svg", {
    version: "1.1",
    xmlns: "http://www.w3.org/2000/svg",
    width: "32",
    height: "32",
    viewBox: "0 0 32 32"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("path", { d: "M24.588 12.274c-1.845 0-3.503 0.769-4.683 2.022-0.5 0.531-1.368 1.16-2.306 1.713 0.441-1.683 1.834-3.803 2.801-4.733 1.239-1.193 2-2.87 2-4.734 0-3.59-2.859-6.503-6.4-6.541-3.541 0.038-6.4 2.951-6.4 6.541 0 1.865 0.761 3.542 2 4.734 0.967 0.93 2.36 3.050 2.801 4.733-0.939-0.553-1.806-1.182-2.306-1.713-1.18-1.253-2.838-2.022-4.683-2.022-3.575 0-6.471 2.927-6.471 6.541s2.897 6.542 6.471 6.542c1.845 0 3.503-0.792 4.683-2.045 0.525-0.558 1.451-1.254 2.447-1.832-0.094 4.615-2.298 8.005-4.541 9.341v1.179h12v-1.179c-2.244-1.335-4.448-4.726-4.541-9.341 0.995 0.578 1.922 1.274 2.447 1.832 1.18 1.253 2.838 2.045 4.683 2.045 3.575 0 6.471-2.928 6.471-6.542s-2.897-6.541-6.471-6.541z" }));
};
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(Club, "Club", "/home/spinoble/Work/tstt/src/app/view/components/icons/Club.js");
  leaveModule(module);
})();

;
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(Club, "Club", "/home/spinoble/Work/tstt/src/app/view/components/icons/Club.js");
  leaveModule(module);
})();

;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./src/app/view/components/icons/Diamond.js":
/*!**************************************************!*\
  !*** ./src/app/view/components/icons/Diamond.js ***!
  \**************************************************/
/*! exports provided: Diamond */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Diamond", function() { return Diamond; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();

(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();



var Diamond = function Diamond() {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("svg", {
    version: "1.1",
    xmlns: "http://www.w3.org/2000/svg",
    width: "32",
    height: "32",
    viewBox: "0 0 32 32",
    color: "red"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("path", { d: "M16 0l-10 16 10 16 10-16z" }));
};
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(Diamond, "Diamond", "/home/spinoble/Work/tstt/src/app/view/components/icons/Diamond.js");
  leaveModule(module);
})();

;
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(Diamond, "Diamond", "/home/spinoble/Work/tstt/src/app/view/components/icons/Diamond.js");
  leaveModule(module);
})();

;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./src/app/view/components/icons/Heart.js":
/*!************************************************!*\
  !*** ./src/app/view/components/icons/Heart.js ***!
  \************************************************/
/*! exports provided: Heart */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Heart", function() { return Heart; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();

(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();



var Heart = function Heart() {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("svg", {
    version: "1.1",
    xmlns: "http://www.w3.org/2000/svg",
    width: "32",
    height: "32",
    viewBox: "0 0 32 32",
    color: "red"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("path", { d: "M23.6 2c-3.363 0-6.258 2.736-7.599 5.594-1.342-2.858-4.237-5.594-7.601-5.594-4.637 0-8.4 3.764-8.4 8.401 0 9.433 9.516 11.906 16.001 21.232 6.13-9.268 15.999-12.1 15.999-21.232 0-4.637-3.763-8.401-8.4-8.401z" }));
};
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(Heart, "Heart", "/home/spinoble/Work/tstt/src/app/view/components/icons/Heart.js");
  leaveModule(module);
})();

;
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(Heart, "Heart", "/home/spinoble/Work/tstt/src/app/view/components/icons/Heart.js");
  leaveModule(module);
})();

;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./src/app/view/components/icons/Spade.js":
/*!************************************************!*\
  !*** ./src/app/view/components/icons/Spade.js ***!
  \************************************************/
/*! exports provided: Spade */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Spade", function() { return Spade; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();

(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();



var Spade = function Spade() {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("svg", {
    version: "1.1",
    xmlns: "http://www.w3.org/2000/svg",
    width: "32",
    height: "32",
    viewBox: "0 0 32 32"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("path", { d: "M25.549 10.88c-6.049-4.496-8.133-8.094-9.549-10.88v0c-0 0-0-0-0-0v0c-1.415 2.785-3.5 6.384-9.549 10.88-10.314 7.665-0.606 18.365 7.93 12.476-0.556 3.654-2.454 6.318-4.381 7.465v1.179h12.001v-1.179c-1.928-1.147-3.825-3.811-4.382-7.465 8.535 5.889 18.244-4.811 7.93-12.476z" }));
};
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(Spade, "Spade", "/home/spinoble/Work/tstt/src/app/view/components/icons/Spade.js");
  leaveModule(module);
})();

;
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(Spade, "Spade", "/home/spinoble/Work/tstt/src/app/view/components/icons/Spade.js");
  leaveModule(module);
})();

;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./src/app/view/components/icons/index.js":
/*!************************************************!*\
  !*** ./src/app/view/components/icons/index.js ***!
  \************************************************/
/*! exports provided: Club, Spade, Heart, Diamond */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Club__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Club */ "./src/app/view/components/icons/Club.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Club", function() { return _Club__WEBPACK_IMPORTED_MODULE_0__["Club"]; });

/* harmony import */ var _Spade__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Spade */ "./src/app/view/components/icons/Spade.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Spade", function() { return _Spade__WEBPACK_IMPORTED_MODULE_1__["Spade"]; });

/* harmony import */ var _Heart__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Heart */ "./src/app/view/components/icons/Heart.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Heart", function() { return _Heart__WEBPACK_IMPORTED_MODULE_2__["Heart"]; });

/* harmony import */ var _Diamond__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Diamond */ "./src/app/view/components/icons/Diamond.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Diamond", function() { return _Diamond__WEBPACK_IMPORTED_MODULE_3__["Diamond"]; });






/***/ }),

/***/ "./src/app/view/components/index.js":
/*!******************************************!*\
  !*** ./src/app/view/components/index.js ***!
  \******************************************/
/*! exports provided: Context, ChoiceForm, AddPlayerForm, PlayerTable, GameTable, GamePanel, NameLabel, UpdateGameModal */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Context__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Context */ "./src/app/view/components/Context.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Context", function() { return _Context__WEBPACK_IMPORTED_MODULE_0__["Context"]; });

/* harmony import */ var _ChoiceForm__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ChoiceForm */ "./src/app/view/components/ChoiceForm.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ChoiceForm", function() { return _ChoiceForm__WEBPACK_IMPORTED_MODULE_1__["ChoiceForm"]; });

/* harmony import */ var _AddPlayerForm__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./AddPlayerForm */ "./src/app/view/components/AddPlayerForm.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AddPlayerForm", function() { return _AddPlayerForm__WEBPACK_IMPORTED_MODULE_2__["AddPlayerForm"]; });

/* harmony import */ var _PlayerTable__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./PlayerTable */ "./src/app/view/components/PlayerTable.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "PlayerTable", function() { return _PlayerTable__WEBPACK_IMPORTED_MODULE_3__["PlayerTable"]; });

/* harmony import */ var _GameTable__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./GameTable */ "./src/app/view/components/GameTable.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "GameTable", function() { return _GameTable__WEBPACK_IMPORTED_MODULE_4__["GameTable"]; });

/* harmony import */ var _GamePanel__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./GamePanel */ "./src/app/view/components/GamePanel.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "GamePanel", function() { return _GamePanel__WEBPACK_IMPORTED_MODULE_5__["GamePanel"]; });

/* harmony import */ var _NameLabel__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./NameLabel */ "./src/app/view/components/NameLabel.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "NameLabel", function() { return _NameLabel__WEBPACK_IMPORTED_MODULE_6__["NameLabel"]; });

/* harmony import */ var _UpdateGameModal__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./UpdateGameModal */ "./src/app/view/components/UpdateGameModal.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "UpdateGameModal", function() { return _UpdateGameModal__WEBPACK_IMPORTED_MODULE_7__["UpdateGameModal"]; });










/***/ }),

/***/ "./src/app/view/components/table/IconedHead.js":
/*!*****************************************************!*\
  !*** ./src/app/view/components/table/IconedHead.js ***!
  \*****************************************************/
/*! exports provided: IconedHead */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IconedHead", function() { return IconedHead; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _cells__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./cells */ "./src/app/view/components/table/cells/index.js");
/* harmony import */ var _icons__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../icons */ "./src/app/view/components/icons/index.js");
(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();

(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();




var style = {
  black: "svgBlack",
  red: "svgRed"
};
var IconedHead = function IconedHead(type) {
  return {
    Header: type === "club" ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_icons__WEBPACK_IMPORTED_MODULE_2__["Club"], null) : type === "spade" ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_icons__WEBPACK_IMPORTED_MODULE_2__["Spade"], null) : type === "heart" ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_icons__WEBPACK_IMPORTED_MODULE_2__["Heart"], null) : react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_icons__WEBPACK_IMPORTED_MODULE_2__["Diamond"], null),
    headerClassName: type === "club" || type === "spade" ? style.red : style.black,
    accessor: type,
    Cell: function Cell(cellInfo) {
      return Object(_cells__WEBPACK_IMPORTED_MODULE_1__["NameCell"])({ cellInfo: cellInfo });
    }
  };
};
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(style, "style", "/home/spinoble/Work/tstt/src/app/view/components/table/IconedHead.js");
  reactHotLoader.register(IconedHead, "IconedHead", "/home/spinoble/Work/tstt/src/app/view/components/table/IconedHead.js");
  leaveModule(module);
})();

;
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(style, "style", "/home/spinoble/Work/tstt/src/app/view/components/table/IconedHead.js");
  reactHotLoader.register(IconedHead, "IconedHead", "/home/spinoble/Work/tstt/src/app/view/components/table/IconedHead.js");
  leaveModule(module);
})();

;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./src/app/view/components/table/IdHead.js":
/*!*************************************************!*\
  !*** ./src/app/view/components/table/IdHead.js ***!
  \*************************************************/
/*! exports provided: IdHead */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IdHead", function() { return IdHead; });
(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();

(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();

var IdHead = {
  Header: "Id",
  accessor: "gameId",
  resizable: false,
  sortable: false,
  width: 40
};
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(IdHead, "IdHead", "/home/spinoble/Work/tstt/src/app/view/components/table/IdHead.js");
  leaveModule(module);
})();

;
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(IdHead, "IdHead", "/home/spinoble/Work/tstt/src/app/view/components/table/IdHead.js");
  leaveModule(module);
})();

;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./src/app/view/components/table/RemovePlayerHead.js":
/*!***********************************************************!*\
  !*** ./src/app/view/components/table/RemovePlayerHead.js ***!
  \***********************************************************/
/*! exports provided: RemovePlayerHead */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RemovePlayerHead", function() { return RemovePlayerHead; });
/* harmony import */ var _cells__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./cells */ "./src/app/view/components/table/cells/index.js");
(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();

(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();



var RemovePlayerHead = {
  Header: "Del",
  resizable: false,
  sortable: false,
  Cell: function Cell(cellInfo) {
    return Object(_cells__WEBPACK_IMPORTED_MODULE_0__["RemovePlayerCell"])({ cellInfo: cellInfo });
  },
  width: 90
};
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(RemovePlayerHead, "RemovePlayerHead", "/home/spinoble/Work/tstt/src/app/view/components/table/RemovePlayerHead.js");
  leaveModule(module);
})();

;
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(RemovePlayerHead, "RemovePlayerHead", "/home/spinoble/Work/tstt/src/app/view/components/table/RemovePlayerHead.js");
  leaveModule(module);
})();

;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./src/app/view/components/table/ScoreHead.js":
/*!****************************************************!*\
  !*** ./src/app/view/components/table/ScoreHead.js ***!
  \****************************************************/
/*! exports provided: ScoreHead */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ScoreHead", function() { return ScoreHead; });
/* harmony import */ var ramda__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ramda */ "ramda");
/* harmony import */ var ramda__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(ramda__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _cells__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./cells */ "./src/app/view/components/table/cells/index.js");
(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();

(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();




var ScoreHead = function ScoreHead(type) {
  return {
    Header: type,
    headerClassName: ramda__WEBPACK_IMPORTED_MODULE_0__["toLower"](type),
    accessor: ramda__WEBPACK_IMPORTED_MODULE_0__["toLower"](type),
    Cell: function Cell(cellInfo) {
      return Object(_cells__WEBPACK_IMPORTED_MODULE_1__["UpdateScoreCell"])({ cellInfo: cellInfo });
    }
  };
};
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(ScoreHead, "ScoreHead", "/home/spinoble/Work/tstt/src/app/view/components/table/ScoreHead.js");
  leaveModule(module);
})();

;
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(ScoreHead, "ScoreHead", "/home/spinoble/Work/tstt/src/app/view/components/table/ScoreHead.js");
  leaveModule(module);
})();

;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./src/app/view/components/table/StatsHead.js":
/*!****************************************************!*\
  !*** ./src/app/view/components/table/StatsHead.js ***!
  \****************************************************/
/*! exports provided: StatsHead */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StatsHead", function() { return StatsHead; });
(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();

(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();

var wins = {
  Header: "Wins",
  accessor: "wins",
  resizable: false
};
var diffs = {
  Header: "Diffs",
  accessor: "diffs",
  sortable: false
};
var points = {
  Header: "Points",
  accessor: "points",
  sortable: false
};
var StatsHead = [wins, diffs, points];
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(wins, "wins", "/home/spinoble/Work/tstt/src/app/view/components/table/StatsHead.js");
  reactHotLoader.register(diffs, "diffs", "/home/spinoble/Work/tstt/src/app/view/components/table/StatsHead.js");
  reactHotLoader.register(points, "points", "/home/spinoble/Work/tstt/src/app/view/components/table/StatsHead.js");
  reactHotLoader.register(StatsHead, "StatsHead", "/home/spinoble/Work/tstt/src/app/view/components/table/StatsHead.js");
  leaveModule(module);
})();

;
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(wins, "wins", "/home/spinoble/Work/tstt/src/app/view/components/table/StatsHead.js");
  reactHotLoader.register(diffs, "diffs", "/home/spinoble/Work/tstt/src/app/view/components/table/StatsHead.js");
  reactHotLoader.register(points, "points", "/home/spinoble/Work/tstt/src/app/view/components/table/StatsHead.js");
  reactHotLoader.register(StatsHead, "StatsHead", "/home/spinoble/Work/tstt/src/app/view/components/table/StatsHead.js");
  leaveModule(module);
})();

;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./src/app/view/components/table/UpdateNameHead.js":
/*!*********************************************************!*\
  !*** ./src/app/view/components/table/UpdateNameHead.js ***!
  \*********************************************************/
/*! exports provided: UpdateNameHead */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateNameHead", function() { return UpdateNameHead; });
/* harmony import */ var _cells__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./cells */ "./src/app/view/components/table/cells/index.js");
(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();

(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();


var UpdateNameHead = {
  Header: "Name",
  accessor: "name",
  resizable: false,
  Cell: function Cell(cellInfo) {
    return Object(_cells__WEBPACK_IMPORTED_MODULE_0__["UpdateNameCell"])({ cellInfo: cellInfo });
  }
};
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(UpdateNameHead, "UpdateNameHead", "/home/spinoble/Work/tstt/src/app/view/components/table/UpdateNameHead.js");
  leaveModule(module);
})();

;
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(UpdateNameHead, "UpdateNameHead", "/home/spinoble/Work/tstt/src/app/view/components/table/UpdateNameHead.js");
  leaveModule(module);
})();

;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./src/app/view/components/table/cells/NameCell.js":
/*!*********************************************************!*\
  !*** ./src/app/view/components/table/cells/NameCell.js ***!
  \*********************************************************/
/*! exports provided: NameCell */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NameCell", function() { return NameCell; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Context__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../Context */ "./src/app/view/components/Context.js");
(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();

(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();






var NameCell = function NameCell(_ref) {
  var cellInfo = _ref.cellInfo;

  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Context__WEBPACK_IMPORTED_MODULE_2__["Context"].Consumer, null, function (_ref2) {
    var getPlayer = _ref2.getPlayer;

    var player = getPlayer(cellInfo.value);
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, player.name);
  });
};
NameCell.propTypes = {
  cellInfo: prop_types__WEBPACK_IMPORTED_MODULE_1__["PropTypes"].object.isRequired
};
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(NameCell, "NameCell", "/home/spinoble/Work/tstt/src/app/view/components/table/cells/NameCell.js");
  leaveModule(module);
})();

;
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(NameCell, "NameCell", "/home/spinoble/Work/tstt/src/app/view/components/table/cells/NameCell.js");
  leaveModule(module);
})();

;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./src/app/view/components/table/cells/RemovePlayerCell.js":
/*!*****************************************************************!*\
  !*** ./src/app/view/components/table/cells/RemovePlayerCell.js ***!
  \*****************************************************************/
/*! exports provided: RemovePlayerCell */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RemovePlayerCell", function() { return RemovePlayerCell; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Context__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../Context */ "./src/app/view/components/Context.js");
(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();

(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();





var RemovePlayerCell = function RemovePlayerCell(_ref) {
  var cellInfo = _ref.cellInfo;

  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Context__WEBPACK_IMPORTED_MODULE_2__["Context"].Consumer, null, function (_ref2) {
    var onRemovePlayer = _ref2.onRemovePlayer;
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
      className: "checkbox",
      type: "checkbox",
      value: cellInfo.original.gameId,
      checked: false,
      onChange: function onChange() {
        return onRemovePlayer(cellInfo.original.gameId);
      }
    });
  });
};
RemovePlayerCell.propTypes = {
  cellInfo: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.object.isRequired
};
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(RemovePlayerCell, "RemovePlayerCell", "/home/spinoble/Work/tstt/src/app/view/components/table/cells/RemovePlayerCell.js");
  leaveModule(module);
})();

;
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(RemovePlayerCell, "RemovePlayerCell", "/home/spinoble/Work/tstt/src/app/view/components/table/cells/RemovePlayerCell.js");
  leaveModule(module);
})();

;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./src/app/view/components/table/cells/UpdateNameCell.js":
/*!***************************************************************!*\
  !*** ./src/app/view/components/table/cells/UpdateNameCell.js ***!
  \***************************************************************/
/*! exports provided: UpdateNameCell */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateNameCell", function() { return UpdateNameCell; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var ramda_adjunct__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ramda-adjunct */ "ramda-adjunct");
/* harmony import */ var ramda_adjunct__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(ramda_adjunct__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _Context__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../Context */ "./src/app/view/components/Context.js");
(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();

(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();






var UpdateNameCell = function UpdateNameCell(_ref) {
  var cellInfo = _ref.cellInfo;

  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Context__WEBPACK_IMPORTED_MODULE_3__["Context"].Consumer, null, function (_ref2) {
    var updatePlayer = _ref2.updatePlayer;

    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      style: {
        textDecoration: cellInfo.original.out ? "line-through" : "none",
        backgroundColor: "#dadada"
      },
      contentEditable: true,
      suppressContentEditableWarning: true,
      onBlur: function onBlur(e) {
        var player = cellInfo.original;
        player.name = e.target.innerHTML;
        if (!Object(ramda_adjunct__WEBPACK_IMPORTED_MODULE_2__["isNilOrEmpty"])(player.name)) {
          updatePlayer(player);
        }
      }
    }, cellInfo.value);
  });
};
UpdateNameCell.propTypes = {
  cellInfo: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.object.isRequired
};
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(UpdateNameCell, "UpdateNameCell", "/home/spinoble/Work/tstt/src/app/view/components/table/cells/UpdateNameCell.js");
  leaveModule(module);
})();

;
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(UpdateNameCell, "UpdateNameCell", "/home/spinoble/Work/tstt/src/app/view/components/table/cells/UpdateNameCell.js");
  leaveModule(module);
})();

;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./src/app/view/components/table/cells/UpdateScoreCell.js":
/*!****************************************************************!*\
  !*** ./src/app/view/components/table/cells/UpdateScoreCell.js ***!
  \****************************************************************/
/*! exports provided: UpdateScoreCell */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateScoreCell", function() { return UpdateScoreCell; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Context__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../Context */ "./src/app/view/components/Context.js");
(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();

(function () {
  var enterModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").enterModule;

  enterModule && enterModule(module);
})();






var UpdateScoreCell = function UpdateScoreCell(_ref) {
  var cellInfo = _ref.cellInfo;

  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Context__WEBPACK_IMPORTED_MODULE_2__["Context"].Consumer, null, function (_ref2) {
    var checked = _ref2.checked,
        makeUpdate = _ref2.makeUpdate;

    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: checked(cellInfo),
      onClick: function onClick() {
        return makeUpdate(cellInfo);
      }
    }, cellInfo.value);
  });
};
UpdateScoreCell.propTypes = {
  cellInfo: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.object.isRequired
};
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(UpdateScoreCell, "UpdateScoreCell", "/home/spinoble/Work/tstt/src/app/view/components/table/cells/UpdateScoreCell.js");
  leaveModule(module);
})();

;
;

(function () {
  var reactHotLoader = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").default;

  var leaveModule = __webpack_require__(/*! react-hot-loader */ "react-hot-loader").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(UpdateScoreCell, "UpdateScoreCell", "/home/spinoble/Work/tstt/src/app/view/components/table/cells/UpdateScoreCell.js");
  leaveModule(module);
})();

;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./src/app/view/components/table/cells/index.js":
/*!******************************************************!*\
  !*** ./src/app/view/components/table/cells/index.js ***!
  \******************************************************/
/*! exports provided: NameCell, UpdateScoreCell, UpdateNameCell, RemovePlayerCell */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _NameCell__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./NameCell */ "./src/app/view/components/table/cells/NameCell.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "NameCell", function() { return _NameCell__WEBPACK_IMPORTED_MODULE_0__["NameCell"]; });

/* harmony import */ var _UpdateScoreCell__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./UpdateScoreCell */ "./src/app/view/components/table/cells/UpdateScoreCell.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "UpdateScoreCell", function() { return _UpdateScoreCell__WEBPACK_IMPORTED_MODULE_1__["UpdateScoreCell"]; });

/* harmony import */ var _UpdateNameCell__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./UpdateNameCell */ "./src/app/view/components/table/cells/UpdateNameCell.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "UpdateNameCell", function() { return _UpdateNameCell__WEBPACK_IMPORTED_MODULE_2__["UpdateNameCell"]; });

/* harmony import */ var _RemovePlayerCell__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./RemovePlayerCell */ "./src/app/view/components/table/cells/RemovePlayerCell.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "RemovePlayerCell", function() { return _RemovePlayerCell__WEBPACK_IMPORTED_MODULE_3__["RemovePlayerCell"]; });






/***/ }),

/***/ "./src/app/view/components/table/index.js":
/*!************************************************!*\
  !*** ./src/app/view/components/table/index.js ***!
  \************************************************/
/*! exports provided: IdHead, UpdateNameHead, RemovePlayerHead, StatsHead, ScoreHead, IconedHead */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _IdHead__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./IdHead */ "./src/app/view/components/table/IdHead.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IdHead", function() { return _IdHead__WEBPACK_IMPORTED_MODULE_0__["IdHead"]; });

/* harmony import */ var _UpdateNameHead__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./UpdateNameHead */ "./src/app/view/components/table/UpdateNameHead.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "UpdateNameHead", function() { return _UpdateNameHead__WEBPACK_IMPORTED_MODULE_1__["UpdateNameHead"]; });

/* harmony import */ var _RemovePlayerHead__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./RemovePlayerHead */ "./src/app/view/components/table/RemovePlayerHead.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "RemovePlayerHead", function() { return _RemovePlayerHead__WEBPACK_IMPORTED_MODULE_2__["RemovePlayerHead"]; });

/* harmony import */ var _StatsHead__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./StatsHead */ "./src/app/view/components/table/StatsHead.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "StatsHead", function() { return _StatsHead__WEBPACK_IMPORTED_MODULE_3__["StatsHead"]; });

/* harmony import */ var _ScoreHead__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./ScoreHead */ "./src/app/view/components/table/ScoreHead.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ScoreHead", function() { return _ScoreHead__WEBPACK_IMPORTED_MODULE_4__["ScoreHead"]; });

/* harmony import */ var _IconedHead__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./IconedHead */ "./src/app/view/components/table/IconedHead.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IconedHead", function() { return _IconedHead__WEBPACK_IMPORTED_MODULE_5__["IconedHead"]; });








/***/ }),

/***/ "./src/app/view/index.js":
/*!*******************************!*\
  !*** ./src/app/view/index.js ***!
  \*******************************/
/*! exports provided: Header, WinnerForm, Register, Tournament, Context */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Header__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Header */ "./src/app/view/Header.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Header", function() { return _Header__WEBPACK_IMPORTED_MODULE_0__["Header"]; });

/* harmony import */ var _WinnerForm__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./WinnerForm */ "./src/app/view/WinnerForm.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "WinnerForm", function() { return _WinnerForm__WEBPACK_IMPORTED_MODULE_1__["WinnerForm"]; });

/* harmony import */ var _Register__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Register */ "./src/app/view/Register.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Register", function() { return _Register__WEBPACK_IMPORTED_MODULE_2__["Register"]; });

/* harmony import */ var _Tournament__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Tournament */ "./src/app/view/Tournament.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Tournament", function() { return _Tournament__WEBPACK_IMPORTED_MODULE_3__["Tournament"]; });

/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components */ "./src/app/view/components/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Context", function() { return _components__WEBPACK_IMPORTED_MODULE_4__["Context"]; });







/***/ }),

/***/ "babel-polyfill":
/*!*********************************!*\
  !*** external "babel-polyfill" ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("babel-polyfill");

/***/ }),

/***/ "prop-types":
/*!*****************************!*\
  !*** external "prop-types" ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("prop-types");

/***/ }),

/***/ "ramda":
/*!************************!*\
  !*** external "ramda" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("ramda");

/***/ }),

/***/ "ramda-adjunct":
/*!********************************!*\
  !*** external "ramda-adjunct" ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("ramda-adjunct");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react-dom":
/*!****************************!*\
  !*** external "react-dom" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-dom");

/***/ }),

/***/ "react-hot-loader":
/*!***********************************!*\
  !*** external "react-hot-loader" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-hot-loader");

/***/ }),

/***/ "react-modal":
/*!******************************!*\
  !*** external "react-modal" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-modal");

/***/ }),

/***/ "react-select":
/*!*******************************!*\
  !*** external "react-select" ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-select");

/***/ }),

/***/ "react-table":
/*!******************************!*\
  !*** external "react-table" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-table");

/***/ }),

/***/ "uuid":
/*!***********************!*\
  !*** external "uuid" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("uuid");

/***/ })

/******/ });
//# sourceMappingURL=app.js.map