import "babel-polyfill";

import React from "react";
import ReactDOM from "react-dom";

import App from "./app/App.jsx";

require.ensure([], () => {
  ReactDOM.render(<App />, document.getElementById("app"));
});
