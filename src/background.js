// This is main process of Electron, started as first thing when your
// app starts. It runs through entire life of your application.
// It doesn't have any windows which you can see on screen, but we can open
// window from here.

import path from "path";
import url from "url";
import { app, Menu } from "electron";
import loadDevtool from "electron-load-devtool";
import { devMenuTemplate } from "./back/menu/dev_menu_template";
import { editMenuTemplate } from "./back/menu/edit_menu_template";
import createWindow from "./back/helpers/window";

let env = { name: "development" };
let mainWindow;
const setApplicationMenu = () => {
  const menus = [editMenuTemplate];
  if (env.name !== "production") {
    menus.push(devMenuTemplate);
  }
  Menu.setApplicationMenu(Menu.buildFromTemplate(menus));
};

// Save userData in separate folders for each environment.
// Thanks to this you can use production and development versions of the app
// on same machine like those are two separate apps.
if (env.name !== "production") {
  const userDataPath = app.getPath("userData");
  app.setPath("userData", `${userDataPath} (${env.name})`);
}

app.on("ready", () => {
  setApplicationMenu();

  mainWindow = createWindow("main", {
    width: 1400,
    height: 800
  });
  mainWindow.loadURL(
    url.format({
      pathname: path.join(__dirname, "app.html"),
      protocol: "file:",
      slashes: true
    })
  );
  //console.log(webContents.getAllWebContents());
  if (env.name === "development") {
    loadDevtool(loadDevtool.REACT_DEVELOPER_TOOLS);
    mainWindow.openDevTools();
  }
});

app.on("window-all-closed", () => {
  app.quit();
});
