import React from "react";
import * as R from "ramda";
import { Player, Game } from "./model";
import { initialState } from "./initialState";

import { Context, Header, Register, WinnerForm, Tournament } from "./view";

const makeId = (pack, randomLength) => {
  let id = Math.floor(Math.random() * randomLength + 1);
  if (pack.includes(id)) {
    id = makeId(pack, randomLength);
  }
  return id;
};
const engin = len => {
  const n = len < 10 ? (len === 7 ? 2 : 1) : len === 10 ? 2 : 3;
  let k = 0;
  if (len / 4 < 3) {
    if (len / 4 < 2) {
      if (len === 6) {
        k = 1;
      }
      if (len === 7) {
        k = 3;
      }
      if (len === 8) {
        k = 4;
      }
    } else if (len === 11) {
      k = 4;
    } else {
      k = 5;
    }
  } else k = 6;
  return R.map(
    v => [
      v,
      v + n > len ? v + n - len : v + n,
      len === 6
        ? v - 3 < 1
          ? v - 3 + len
          : v - 3
        : v - 2 < 1
          ? v - 2 + len
          : v - 2,
      v - k < 1 ? v - k + len : v - k
    ],
    R.range(1, len + 1)
  );
};

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      ...initialState,
      addPlayer: this.addPlayer,
      updatePlayer: this.updatePlayer,
      getPlayer: this.getPlayer,
      getGamesList: this.getGamesList,
      getPlayersList: this.getPlayersList,
      makeUpdate: this.makeUpdate,
      onChangeSelect: this.onChangeSelect,
      onChangeSelected: this.onChangeSelected,
      onRemovePlayer: this.onRemovePlayer,
      toggleTab: this.toggleTab,
      changeStage: this.changeStage,
      len: this.len,
      checked: this.checked
    };
  }

  toggleTeamList = teamList => this.setState({ teamList });
  togglePlayers = players => this.setState({ players });
  toggleGames = games => this.setState({ games });
  toggleTab = tab => this.setState({ tab });
  toggleStage = stage => this.setState({ stage });
  toggleModal = modal => this.setState({ modal });
  toggleUpdate = update => this.setState({ update });
  toggleProgress = game => this.setState({ game });
  toggleCounter = counter => this.setState({ counter });
  toggleChoice = choice => this.setState({ choice });
  toggleSortedPlayerList = sortedPlayerList =>
    this.setState({ sortedPlayerList });
  checked = cellInfo => {
    const { modal, game } = this.state;
    return modal &&
      cellInfo.column.id === game.column.id &&
      cellInfo.viewIndex === game.viewIndex
      ? "checked"
      : "";
  };
  updatePlayer = player =>
    this.setState(state => ({
      ...state,
      players: {
        ...state.players,
        [player.id]: player
      }
    }));
  updateGame = game =>
    this.setState(state => ({
      ...state,
      games: {
        ...state.games,
        [game.id]: game
      }
    }));
  addPlayer = name => {
    const player = Player.new(name);
    player.gameId = this.len() + 1;
    this.updatePlayer(player);
  };
  onRemovePlayer = gameId => {
    const player = this.getPlayer(gameId);
    const gamers = R.filter(gamer => gamer !== player, this.state.players);
    this.togglePlayers(gamers);
  };
  addGame = game => {
    const play = Game.new(game);
    this.updateGame(play);
  };
  getPlayer = id => {
    const { players } = this.state;
    let player = {};
    Object.values(players).map(gamer => {
      if (gamer.gameId === id) {
        player = gamer;
      }
    });
    return player;
  };
  getPlayersList = () => Object.values(this.state.players).map(p => p);
  len = () => this.getPlayersList().length;
  getGamesList = () => Object.values(this.state.games).map(g => g);
  changeStage = stage => {
    this.toggleGames({});
    if (!(stage % 2)) {
      if (stage === 4) {
        this.makeSortedList(false);
      } else {
        this.makeGame();
      }
      this.toggleTab(2);
    }
    this.toggleStage(stage);
  };
  makeUpdate = cellInfo => {
    const { black, red } = cellInfo.original;
    const { stage } = this.state;

    if (stage === 4 && black !== red) {
      return {};
    } else {
      this.toggleUpdate(black === red ? false : { black, red });
      this.toggleProgress(cellInfo);
      this.toggleModal(true);
    }
  };
  makeGame = () => {
    const { players, len } = this.state;
    let pack = [];
    engin(len()).forEach((element, ind) => {
      this.addGame({
        id: ind + 1,
        club: element[0],
        spade: element[1],
        heart: element[2],
        diamond: element[3]
      });
    });
    this.togglePlayers(
      R.mapObjIndexed(player => {
        let id = makeId(pack, len());
        pack = [...pack, id];
        player.gameId = id;
        return player;
      }, players)
    );
    this.toggleCounter(len());
  };
  makePoints = selectedOption => {
    const { counter, game, update, players, games } = this.state;
    let play = game.original;
    const columName = game.column.id;
    [play.club, play.spade, play.heart, play.diamond].forEach((id, index) => {
      this.updatePlayer(this.makePlayersPoints({ id, index, selectedOption }));
    });
    columName === "red" ? (play.black = 6) : (play.red = 6);
    play[columName] = selectedOption;
    !update && this.toggleCounter(counter + 1);
    this.toggleProgress(false);
    this.updateGame(play);
    if (counter === this.getGamesList().length + 1) {
      const data = {
        players,
        games
      };
      localStorage.setItem("data", JSON.stringify(data));
      this.toggleUpdate(false);
      this.changeStage(3);
      this.toggleTab(1);
      this.toggleCounter(1);
      this.toggleModal(true);
    }
  };
  makePlayersPoints = ({ id, index, selectedOption }) => {
    const { update, game } = this.state;
    const columName = game.column.id;
    let v = 6 - Number(selectedOption);
    let gamer = this.getPlayer(id);
    if (update) {
      const { black, red } = update;
      const diff = black - red;
      if (columName === "red") {
        return Player.updateRed(index, gamer, diff, v);
      } else {
        return Player.updateBlack(index, gamer, diff, v);
      }
    } else {
      if (columName === "red") {
        return Player.red(index, gamer, v);
      } else {
        return Player.black(index, gamer, v);
      }
    }
  };
  makeKoPreGame = selectedOption => {
    const { game } = this.state;
    const columName = game.column.id;
    let play = game.original;
    let list = {
      winners: [],
      loosers: []
    };
    const club = this.getPlayer(play.club);
    const spade = this.getPlayer(play.spade);
    const heart = this.getPlayer(play.heart);
    const diamond = this.getPlayer(play.diamond);

    if (columName === "red") {
      list.winners = [club, spade];
      list.loosers = [heart, diamond];
      play.black = 6;
    } else {
      list.winners = [heart, diamond];
      list.lossers = [club, spade];
      play.red = 6;
    }
    for (let i = 0; i < 2; i++) {
      let p = list.loosers[i];
      p.out = true;
      this.updatePlayer(p);
    }
    play[columName] = selectedOption;
    this.toggleProgress(false);
    this.toggleChoice(true);
    this.makeSortedList(list.winners);
    this.updateGame(play);
  };
  makeSortedList = list => {
    const { sortedPlayerList } = this.state;
    const _sortedPlayerList = !list
      ? R.sortWith([R.descend(R.prop("wins")), R.descend(R.prop("points"))])(
          this.getPlayersList()
        )
      : [...list, ...sortedPlayerList];
    if (sortedPlayerList.length < 2 && list) {
      this.makeWin();
    } else {
      this.makeTeam(_sortedPlayerList);
      this.toggleChoice(true);
    }
    this.toggleSortedPlayerList(_sortedPlayerList);
  };
  makeTeam = sortedPlayerList => {
    const { counter } = this.state;
    const len = sortedPlayerList.length;
    let l = sortedPlayerList;
    let gamers = [];
    for (let i = 1; i < 3; i++) {
      let { gameId, name } = sortedPlayerList.pop();
      gamers.push({
        value: {
          id: gameId,
          ind: gamers.length,
          i: counter
        },
        label: name
      });
    }
    if (len % 4 === 0 && len / 4 === 0) {
      for (let i = 1; i < 3; i++) {
        let { gameId, name } = sortedPlayerList.shift();
        gamers.push({
          value: {
            id: gameId,
            ind: gamers.length,
            i: counter
          },
          label: name
        });
      }
    } else {
      for (let i = 1; i < 3; i++) {
        let { gameId, name } = sortedPlayerList.pop();
        gamers.push({
          value: {
            id: gameId,
            ind: gamers.length,
            i: counter
          },
          label: name
        });
      }
      console.log(len % 4 === 0 || len / 4 === 0);
    }
    this.toggleCounter(counter + 1);
    this.toggleTeamList(gamers);
    this.toggleSortedPlayerList(l);
  };
  makeWin = () => {
    const { players, games } = this.state;
    const data = {
      players,
      games
    };
    localStorage.setItem("koPlay", JSON.stringify(data));
    this.toggleStage(5);
  };
  onChangeSelect = selectedOption => {
    this.state.stage < 4
      ? this.makePoints(selectedOption)
      : this.makeKoPreGame(selectedOption);
    this.toggleModal(false);
  };
  onChangeSelected = selectedOption => {
    const { teamList, game } = this.state;
    const { id, ind, i } = selectedOption;
    const nextId = ind < 2 ? Math.abs(ind - 1) : game ? 3 - ind : 5 - ind;
    if (game) {
      const { club, spade } = game;
      const koGame = {
        id: i,
        heart: id,
        diamond: teamList[nextId].value.id,
        club,
        spade
      };
      this.addGame(koGame);
      this.toggleChoice(false);
      this.toggleProgress(false);
      this.toggleTeamList([]);
    } else {
      const black = {
        club: id,
        spade: teamList[nextId].value.id
      };
      const forDeletion = [selectedOption.ind, nextId];
      const teams = teamList.filter(
        item => !forDeletion.includes(item.value.ind)
      );
      this.toggleProgress(black);
      this.toggleTeamList(teams);
    }
  };
  render() {
    const { tab, stage, choice, teamList, sortedPlayerList } = this.state;
    return (
      <Context.Provider value={this.state}>
        <Header />
        {!(stage % 2) ? (
          <Tournament tab={tab} choice={choice} teamList={teamList} />
        ) : stage === 5 ? (
          <WinnerForm sortedPlayerList={sortedPlayerList} />
        ) : (
          <Register />
        )}
      </Context.Provider>
    );
  }
}

export default App;
