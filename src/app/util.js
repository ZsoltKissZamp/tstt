import { Player } from "./model";

const namen = [
  "aa",
  "bb",
  "cc",
  "dd",
  "ee",
  "ff",
  "gg",
  "hh",
  "ii",
  "jj",
  "kk"
];
let demoPlayers = {};

for (const name of namen) {
  const player = Player.new(name);
  demoPlayers = { ...demoPlayers, [player.id]: player };
}
export default demoPlayers;
