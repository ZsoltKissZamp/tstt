import { v4 as uuid } from "uuid";

export const Player = {
  new: name => ({
    id: uuid(),
    name: name,
    wins: 0,
    diffs: 0,
    points: 0,
    gameId: 0,
    out: false
  }),
  black: (index, gamer, v) => {
    let player =
      index >= 2
        ? {
            ...gamer,
            diffs: gamer.diffs + v,
            wins: gamer.wins + 1
          }
        : {
            ...gamer,
            diffs: gamer.diffs - v
          };
    player.points = player.diffs + player.wins;
    return player;
  },
  red: (index, gamer, v) => {
    let player =
      index < 2
        ? {
            ...gamer,
            diffs: gamer.diffs + v,
            wins: gamer.wins + 1
          }
        : {
            ...gamer,
            diffs: gamer.diffs - v
          };
    player.points = player.diffs + player.wins;
    return player;
  },
  updateRed: (index, gamer, diff, v) => {
    let player =
      index < 2
        ? {
            ...gamer,
            diffs: gamer.diffs - diff + v,
            wins: diff < 0 ? gamer.wins + 1 : gamer.wins
          }
        : {
            ...gamer,
            diffs: gamer.diffs + diff - v,
            wins: diff < 0 ? gamer.wins - 1 : gamer.wins
          };
    player.points = player.diffs + player.wins;
    return player;
  },
  updateBlack: (index, gamer, diff, v) => {
    let player =
      index >= 2
        ? {
            ...gamer,
            diffs: gamer.diffs + diff + v,
            wins: diff > 0 ? gamer.wins + 1 : gamer.wins
          }
        : {
            ...gamer,
            diffs: gamer.diffs - diff - v,
            wins: diff > 0 ? gamer.wins - 1 : gamer.wins
          };
    player.points = player.diffs + player.wins;
    return player;
  }
};
