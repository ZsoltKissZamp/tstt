import { v4 as uuid } from "uuid";

export const Game = {
  new: (game = {}) => ({
    id: uuid(),
    club: game.club,
    spade: game.spade,
    heart: game.heart,
    diamond: game.diamond,
    black: 0,
    red: 0,
    gameId: game.id
  })
};
