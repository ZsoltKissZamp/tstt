import React from "react";
import { AddPlayerForm, PlayerTable } from "./components";

export const Register = () => {
  return (
    <div className="RegisterView">
      <AddPlayerForm />
      <PlayerTable />
    </div>
  );
};
