import React from "react";
import PropTypes from "prop-types";

export const TogglerButton = ({ disabled, onClick, children }) => {
  return (
    <button
      className={"inputname"}
      type="button"
      disabled={disabled}
      onClick={onClick}
    >
      {children}
    </button>
  );
};
TogglerButton.propTypes = {
  disabled: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired,
  children: PropTypes.string.isRequired
};
