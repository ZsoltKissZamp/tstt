import React from "react";
import ReactTable from "react-table";

import { Context } from "./Context";
import { IdHead, ScoreHead, IconedHead } from "./table";

export const GameTable = () => {
  return (
    <Context.Consumer>
      {({ getGamesList }) => {
        const columns = [
          IdHead,
          IconedHead("club"),
          IconedHead("spade"),
          IconedHead("heart"),
          IconedHead("diamond"),
          ScoreHead("black"),
          ScoreHead("red")
        ];

        return (
          <ReactTable
            className="center -striped bordered"
            showPagination={false}
            minRows={1}
            data={getGamesList()}
            resizable={false}
            defaultSorted={[
              {
                id: "gameId",
                asc: true
              }
            ]}
            sortable={false}
            columns={columns}
          />
        );
      }}
    </Context.Consumer>
  );
};
