import React from "react";
import PropTypes from "prop-types";

import { Club, Heart } from "./icons";
import { TeamSelect } from "./TeamSelect";

export const ChoiceForm = ({ state }) => {
  return (
    <div className={"choice"}>
      {state ? <Club /> : <Heart />}
      <TeamSelect />
    </div>
  );
};

ChoiceForm.propTypes = {
  state: PropTypes.bool
};
