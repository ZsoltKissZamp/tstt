import React from "react";
import { isNilOrEmpty } from "ramda-adjunct";
import { Context } from "./Context";
import { NameInput } from "./NameInput";

export class AddPlayerForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { name: "" };
    this.handleChange = this.handleChange.bind(this);
    this.resetState = this.resetState.bind(this);
  }
  handleChange = event => this.setState({ name: event.target.value });
  resetState = () => this.setState({ name: "" });

  render() {
    const { name } = this.state;
    return (
      <Context.Consumer>
        {({ addPlayer, stage, len }) => {
          return (
            <form
              onSubmit={event => {
                event.preventDefault();
                if (!isNilOrEmpty(name)) {
                  addPlayer(name);
                }
                this.resetState();
              }}
            >
              <NameInput
                aF={true}
                type="text"
                v={name}
                oC={this.handleChange}
                d={stage === 3 && len() > 5 && !(len() % 2) ? true : false}
              />
            </form>
          );
        }}
      </Context.Consumer>
    );
  }
}
