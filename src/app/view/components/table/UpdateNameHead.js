import { UpdateNameCell } from "./cells";
export const UpdateNameHead = {
  Header: "Name",
  accessor: "name",
  resizable: false,
  Cell: cellInfo => UpdateNameCell({ cellInfo })
};
