export const IdHead = {
  Header: "Id",
  accessor: "gameId",
  resizable: false,
  sortable: false,
  width: 40
};
