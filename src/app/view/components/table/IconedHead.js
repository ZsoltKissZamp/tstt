import React from "react";
import { NameCell } from "./cells";
import { Club, Spade, Heart, Diamond } from "../icons";
const style = {
  black: "svgBlack",
  red: "svgRed"
};
export const IconedHead = type => {
  return {
    Header:
      type === "club" ? (
        <Club />
      ) : type === "spade" ? (
        <Spade />
      ) : type === "heart" ? (
        <Heart />
      ) : (
        <Diamond />
      ),
    headerClassName:
      type === "club" || type === "spade" ? style.red : style.black,
    accessor: type,
    Cell: cellInfo => NameCell({ cellInfo })
  };
};
