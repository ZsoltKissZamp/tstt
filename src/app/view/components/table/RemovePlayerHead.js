import { RemovePlayerCell } from "./cells";

export const RemovePlayerHead = {
  Header: "Del",
  resizable: false,
  sortable: false,
  Cell: cellInfo => RemovePlayerCell({ cellInfo }),
  width: 90
};
