const wins = {
  Header: "Wins",
  accessor: "wins",
  resizable: false
};
const diffs = {
  Header: "Diffs",
  accessor: "diffs",
  sortable: false
};
const points = {
  Header: "Points",
  accessor: "points",
  sortable: false
};
export const StatsHead = [wins, diffs, points];
