import * as R from "ramda";
import { UpdateScoreCell } from "./cells";

export const ScoreHead = type => ({
  Header: type,
  headerClassName: R.toLower(type),
  accessor: R.toLower(type),
  Cell: cellInfo => UpdateScoreCell({ cellInfo })
});
