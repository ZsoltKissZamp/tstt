export { IdHead } from "./IdHead";
export { UpdateNameHead } from "./UpdateNameHead";
export { RemovePlayerHead } from "./RemovePlayerHead";
export { StatsHead } from "./StatsHead";
export { ScoreHead } from "./ScoreHead";
export { IconedHead } from "./IconedHead";
