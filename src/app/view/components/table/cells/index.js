export { NameCell } from "./NameCell";
export { UpdateScoreCell } from "./UpdateScoreCell";
export { UpdateNameCell } from "./UpdateNameCell";
export { RemovePlayerCell } from "./RemovePlayerCell";
