import React from "react";
import PropTypes from "prop-types";
import { Context } from "../../Context";

export const RemovePlayerCell = ({ cellInfo }) => {
  return (
    <Context.Consumer>
      {({ onRemovePlayer }) => (
        <input
          className={"checkbox"}
          type="checkbox"
          value={cellInfo.original.gameId}
          checked={false}
          onChange={() => onRemovePlayer(cellInfo.original.gameId)}
        />
      )}
    </Context.Consumer>
  );
};
RemovePlayerCell.propTypes = {
  cellInfo: PropTypes.object.isRequired
};
