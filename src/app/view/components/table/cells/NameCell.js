import React from "react";
import { PropTypes } from "prop-types";

import { Context } from "../../Context";

export const NameCell = ({ cellInfo }) => {
  return (
    <Context.Consumer>
      {({ getPlayer }) => {
        const player = getPlayer(cellInfo.value);
        return <div>{player.name}</div>;
      }}
    </Context.Consumer>
  );
};
NameCell.propTypes = {
  cellInfo: PropTypes.object.isRequired
};
