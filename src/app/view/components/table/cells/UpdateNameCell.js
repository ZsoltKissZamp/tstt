import React from "react";
import PropTypes from "prop-types";
import { isNilOrEmpty } from "ramda-adjunct";
import { Context } from "../../Context";

export const UpdateNameCell = ({ cellInfo }) => {
  return (
    <Context.Consumer>
      {({ updatePlayer }) => {
        return (
          <div
            style={{
              textDecoration: cellInfo.original.out ? "line-through" : "none",
              backgroundColor: "#dadada"
            }}
            contentEditable={true}
            suppressContentEditableWarning={true}
            onBlur={e => {
              const player = cellInfo.original;
              player.name = e.target.innerHTML;
              if (!isNilOrEmpty(player.name)) {
                updatePlayer(player);
              }
            }}
          >
            {cellInfo.value}
          </div>
        );
      }}
    </Context.Consumer>
  );
};
UpdateNameCell.propTypes = {
  cellInfo: PropTypes.object.isRequired
};
