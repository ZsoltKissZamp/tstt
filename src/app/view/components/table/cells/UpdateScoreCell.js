import React from "react";
import PropTypes from "prop-types";

import { Context } from "../../Context";

export const UpdateScoreCell = ({ cellInfo }) => {
  return (
    <Context.Consumer>
      {({ checked, makeUpdate }) => {
        return (
          <div
            className={checked(cellInfo)}
            onClick={() => makeUpdate(cellInfo)}
          >
            {cellInfo.value}
          </div>
        );
      }}
    </Context.Consumer>
  );
};
UpdateScoreCell.propTypes = {
  cellInfo: PropTypes.object.isRequired
};
