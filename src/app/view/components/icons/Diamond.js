import React from "react";

export const Diamond = () => {
  return (
    <svg
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
      width="32"
      height="32"
      viewBox="0 0 32 32"
      color="red"
    >
      <path d="M16 0l-10 16 10 16 10-16z" />
    </svg>
  );
};
