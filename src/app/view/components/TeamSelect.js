import React from "react";
import Select from "react-select";
import { Context } from "./Context";

const customTeam = {
  control: () => ({
    display: "none"
  })
};
export const TeamSelect = () => {
  return (
    <Context.Consumer>
      {({ teamList, onChangeSelected }) => {
        return (
          <Select
            styles={customTeam}
            menuIsOpen={true}
            onChange={e => onChangeSelected(e.value)}
            options={teamList}
          />
        );
      }}
    </Context.Consumer>
  );
};
