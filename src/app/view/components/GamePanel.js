import React from "react";
import { Context } from "./Context";
import { TogglerButton } from "./TogglerButton";
const text = {
  button: {
    underMinimum: "Add Player",
    start: "Start Game",
    startKO: "Start KO Game",
    default: "Add or remove Player"
  },
  tab: {
    player: "Players",
    game: "Games"
  }
};
export const GamePanel = () => {
  return (
    <Context.Consumer>
      {({ stage, changeStage, tab, toggleTab, len }) => {
        return stage === 1 ? (
          <TogglerButton
            disabled={len() < 6 ? true : false}
            onClick={() => changeStage(2)}
          >
            {len() < 6 ? text.button.underMinimum : text.button.start}
          </TogglerButton>
        ) : stage === 3 && !(len() % 2) ? (
          <TogglerButton disabled={false} onClick={() => changeStage(4)}>
            {text.button.startKO}
          </TogglerButton>
        ) : (
          !(stage % 2) && (
            <div>
              <TogglerButton
                disabled={tab === 2 ? false : true}
                onClick={() => toggleTab(1)}
              >
                {text.tab.player}
              </TogglerButton>
              <TogglerButton
                disabled={tab === 1 ? false : true}
                onClick={() => toggleTab(2)}
              >
                {text.tab.game}
              </TogglerButton>
            </div>
          )
        );
      }}
    </Context.Consumer>
  );
};
