import React from "react";
import ReactTable from "react-table";

import * as R from "ramda";

import { Context } from "./Context";

import { IdHead, UpdateNameHead, RemovePlayerHead, StatsHead } from "./table";

export const PlayerTable = () => {
  return (
    <Context.Consumer>
      {({ getPlayersList, stage }) => {
        let columns = [IdHead, UpdateNameHead];
        columns =
          stage % 2
            ? [...columns, RemovePlayerHead]
            : R.concat(columns, StatsHead);
        return (
          <ReactTable
            className={`center -striped bordered ${
              stage !== 2 ? (stage !== 4 ? "suplied" : "") : ""
            }`}
            showPagination={false}
            minRows={6}
            data={getPlayersList()}
            defaultSorted={
              stage > 2
                ? [
                    {
                      id: "wins",
                      desc: true
                    },
                    {
                      id: "points",
                      desc: true
                    }
                  ]
                : [
                    {
                      id: "gameId",
                      asc: true
                    }
                  ]
            }
            columns={columns}
          />
        );
      }}
    </Context.Consumer>
  );
};
