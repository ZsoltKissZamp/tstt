import React from "react";
import Modal from "react-modal";
import { ResultSelect } from "./ResultSelect";
import { Context } from "./Context";

const modalStyle = {
  overlay: {
    backgroundColor: "rgba(85, 139, 27, 0.55)"
  },
  content: {
    position: "fixed",
    top: "5rem",
    width: "10%",
    left: "none",
    right: 0,
    border: 0,
    pading: 0,
    background: ""
  }
};

Modal.setAppElement("#app");

export const UpdateGameModal = () => {
  return (
    <Context.Consumer>
      {({ modal }) => {
        return (
          <Modal
            isOpen={modal}
            style={modalStyle}
            shouldCloseOnOverlayClick={false}
            ariaHideApp={true}
          >
            <ResultSelect />
          </Modal>
        );
      }}
    </Context.Consumer>
  );
};
