import React from "react";
import Select from "react-select";
import { Context } from "./Context";

const customStyles = {
  option: (base, state) => ({
    ...base,
    borderBottom: "1px dotted pink",
    color: state.isFullscreen ? "red" : "blue",
    padding: 5
  }),
  control: () => ({
    // none of react-selects styles are passed to <View />
    display: "none"
  }),
  singleValue: (base, state) => {
    const opacity = state.isDisabled ? 0.5 : 1;
    const transition = "opacity 300ms";

    return { ...base, opacity, transition };
  }
};

const options = [
  { value: 0, label: "0" },
  { value: 1, label: "1" },
  { value: 2, label: "2" },
  { value: 3, label: "3" },
  { value: 4, label: "4" },
  { value: 5, label: "5" }
];

export const ResultSelect = () => {
  return (
    <Context.Consumer>
      {({ onChangeSelect }) => {
        return (
          <Select
            styles={customStyles}
            menuIsOpen={true}
            onChange={e => onChangeSelect(e.value)}
            options={options}
          />
        );
      }}
    </Context.Consumer>
  );
};
