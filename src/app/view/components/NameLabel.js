import React from "react";
import PropTypes from "prop-types";

export const NameLabel = ({ name }) => {
  return <label className={"winner"}>{name}</label>;
};
NameLabel.propTypes = {
  name: PropTypes.string.isRequired
};
