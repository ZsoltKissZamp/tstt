import React from "react";
import PropTypes from "prop-types";

export const NameInput = ({ aF, v, oC, d }) => (
  <input autoFocus={aF} type="text" value={v} onChange={oC} disabled={d} />
);

NameInput.propTypes = {
  aF: PropTypes.bool,
  v: PropTypes.string,
  oC: PropTypes.func,
  d: PropTypes.bool
};
