import React from "react";
import PropTypes from "prop-types";

import {
  PlayerTable,
  GameTable,
  ChoiceForm,
  UpdateGameModal
} from "./components";

export const Tournament = ({ tab, teamList, choice }) => {
  return choice ? (
    <ChoiceForm state={teamList.length === 2 ? false : true} />
  ) : tab === 1 ? (
    <PlayerTable />
  ) : (
    <div>
      <GameTable />
      <UpdateGameModal />
    </div>
  );
};

Tournament.propTypes = {
  tab: PropTypes.number.isRequired,
  choice: PropTypes.bool.isRequired,
  teamList: PropTypes.array.isRequired
};
