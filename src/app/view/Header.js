import React from "react";
import { GamePanel } from "./components";

export const Header = () => {
  return (
    <div className={"header"}>
      <h3>{"Table Soccer Tournament Tool"}</h3>
      <div className={"navi"}>
        <GamePanel />
      </div>
    </div>
  );
};
