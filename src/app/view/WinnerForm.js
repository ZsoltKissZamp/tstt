import React from "react";
import PropTypes from "prop-types";
import { NameLabel } from "./components";
export const WinnerForm = ({ sortedPlayerList }) => {
  return (
    <div className={"carry"}>
      <div>Congratulation Today the winner are: </div>
      <div className={"holder"}>
        <NameLabel name={sortedPlayerList[0].name} />
        <NameLabel name={sortedPlayerList[1].name} />
      </div>
      <button>Get Pdf</button>
    </div>
  );
};
WinnerForm.propTypes = {
  sortedPlayerList: PropTypes.array
};
