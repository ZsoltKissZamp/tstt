import demoPlayers from "./util";

const dev = true;

export const initialState = {
  players: dev ? demoPlayers : {},
  games: {},
  player: {},
  game: {},
  sortedPlayerList: [],
  teamList: [],
  stage: 1,
  tab: 1,
  counter: 1,
  modal: false,
  update: false,
  choice: false
};
